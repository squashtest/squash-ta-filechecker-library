/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import java.text.DecimalFormat;

import org.junit.Test;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

import static org.junit.Assert.*;

/**
 * @author amd
 */
//Pb: les nombres négatifs n'auront pas le même nombre de caractères que les positifs.
public class DoubleFormatterTest {

    private DecimalFormatter formatter;

    private String actual, expected;

    private Double myDouble;

    private DecimalFormat decimalFormat;
    
    @Test
    public void nominalNegative() throws InvalidSyntaxException {
        
        decimalFormat = new DecimalFormat("0000.00");
        formatter = new DecimalFormatter(decimalFormat, 7, true);
        //actual in SUT
        myDouble = new Double(-00000789.600000);
        actual = formatter.format(myDouble).toString();
        
        //expected
        expected = "-0789,60";
        
        assertEquals(expected, actual);
    }

    @Test
    public void nominalPositiveSemicolon() throws InvalidSyntaxException {
        
        decimalFormat = new DecimalFormat("0000.00;00000.00");
        formatter = new DecimalFormatter(decimalFormat, 7, true);
  
        //actual in SUT
        myDouble = new Double(789.6);
        actual = formatter.format(myDouble).toString();
        
        //expected
        expected = "0789,60";

        assertEquals(expected, actual);
    }
    
    @Test
    public void nominalPositive() throws InvalidSyntaxException {
        
        decimalFormat = new DecimalFormat("0000.00");
        formatter = new DecimalFormatter(decimalFormat, 7, true);
        
        //actual
        myDouble = new Double(789.6);
        actual = formatter.format(myDouble).toString();
        
        //expected
        expected = "0789,60";

        assertEquals(expected, actual);
    }
 
    @Test
    public void invalidFormatNAN() throws InvalidSyntaxException {
        try {
            decimalFormat = new DecimalFormat("toto");
            formatter = new DecimalFormatter(decimalFormat, 9, true);
            myDouble = new Double(98.6);
            formatter.format(myDouble);
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Le champ formatté n'a pas le nombre de caractères requis.", e.getMessage());
        }
    }

    @Test
    public void invalidValuePositive() {
        try {
            decimalFormat = new DecimalFormat("00000.00;00000.00");
            formatter = new DecimalFormatter(decimalFormat, 9, true);
            myDouble = new Double(79898098098098098.6);
            formatter.format(myDouble);
            fail();
        } catch (InvalidSyntaxException e) {
            assertEquals("Le champ formatté n'a pas le nombre de caractères requis.", e.getMessage());
        }
    }

    @Test
    public void invalidValueNegative() {
        try {
            decimalFormat = new DecimalFormat("00000.00;00000.00");
            formatter = new DecimalFormatter(decimalFormat, 8, true);
            myDouble = new Double(-79898098098098098.6);
            formatter.format(myDouble);
            fail();
        } catch (InvalidSyntaxException e) {
            assertEquals("Le champ formatté n'a pas le nombre de caractères requis.", e.getMessage());
        }
    }
    @Test
    public void noValueAndNullable() throws InvalidSyntaxException {
        decimalFormat = new DecimalFormat("00000.00;00000.00");
        formatter = new DecimalFormatter(decimalFormat, 9, true);
        expected = "         ";
        actual = formatter.format(null).toString();
        assertEquals(expected, actual);
    }

    @Test
    public void noValueAndNotNullable() {
        try {
            decimalFormat = new DecimalFormat("00000.00;00000.00");
            formatter = new DecimalFormatter(decimalFormat, 9, false);
            formatter.format(null);
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Champ obligatoire.", e.getMessage());
        }
    }
    
}
