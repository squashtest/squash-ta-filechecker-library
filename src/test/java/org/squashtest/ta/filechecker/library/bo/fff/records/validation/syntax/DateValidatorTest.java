/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


/**
 * @author amd
 *
 */
//ne détecte pas les dates incorrectes comme "32/12/2009", qui est considérée comme 01/01/2010.
public class DateValidatorTest {

    private DateValidator validator;
    
    @Test
    public void nominal() throws InvalidSyntaxException, ValidatorInitializationException{
        validator = new DateValidator("dd/MM/yyyy", true);
        validator.validate(new StringBuffer("03/10/2009"));
    }
    
    @Test
    public void invalidFormat() {
        try{
            validator = new DateValidator("toto", true);
            assertTrue("Exception non levée.", false);
        }catch (ValidatorInitializationException e){
            assertEquals("\"toto\" is not a valid date format.", e.getMessage());
        }
    }
    
    @Test
    public void invalidValue1() throws ValidatorInitializationException{
        validator = new DateValidator("dd/MM/yyyy", true);
        try{
        validator.validate(new StringBuffer("03-10-2009"));
        assertTrue("Exception non levée.", false);
        }catch(InvalidSyntaxException e){
            assertEquals("\"03-10-2009\" isn't a date to the format: \"dd/MM/yyyy\".", e.getMessage());
        }
    }
    
    @Test
    public void blankValueAndNullable() throws ValidatorInitializationException, InvalidSyntaxException {
        validator = new DateValidator("dd/MM/yyyy", true);
        validator.validate(new StringBuffer("          "));
    }
    
    @Test
    public void blankValueAndNotNullable() throws ValidatorInitializationException{
        validator = new DateValidator("dd/MM/yyyy", false);
        try{
            validator.validate(new StringBuffer("          "));
            assertTrue("Exception non levée.", false);
        }catch(InvalidSyntaxException e){
            assertEquals("Champ obligatoire.", e.getMessage());
        }
    }
    
}
