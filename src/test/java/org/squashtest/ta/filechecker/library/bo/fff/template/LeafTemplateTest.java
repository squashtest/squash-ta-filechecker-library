/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.template;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AbstractFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.ConstantFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.FixedFieldType;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.IdField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.InputField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.fff.template.LeafTemplate;

public class LeafTemplateTest {

    private LeafTemplate leafTemplate;
    
    @Before
    public void setUp(){
        
        IdField roff1 = new IdField(null, null, 1, 5, new StringBuffer("12345"));
        InputField<String> roff2 = new InputField<String>(null, null, 6, 5, null, null,FixedFieldType.text);
        IdField roff3 = new IdField(null, null, 11, 5, new StringBuffer("54321"));

        List<AbstractFixedField> fields = new ArrayList<AbstractFixedField>();
        fields.add(roff1);
        fields.add(roff2);
        fields.add(roff3);
        
        LeafRecord leafRecord = new LeafRecord(null, null, null, fields, null, null);
        
        List<IdField> ids = new ArrayList<IdField>();
        ids.add(roff1);
        ids.add(roff3);
        
        leafTemplate = new LeafTemplate(leafRecord, ids, 15);
    }
    
    @Test
    public void match(){
        boolean matches = leafTemplate.matches("12345     54321");
        assertTrue("", matches);
    }
    
    @Test
    public void matchTooShort(){
        boolean matches = leafTemplate.matches("12345     5");
        assertFalse("", matches);
    }
    
    @Test
    public void matchWrongId(){
        boolean matches = leafTemplate.matches("12345     11111");
        assertFalse("", matches);
    }
    
}
