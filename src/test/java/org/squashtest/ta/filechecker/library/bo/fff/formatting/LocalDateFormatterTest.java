/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

/**
 * @author amd
 *
 */
public class LocalDateFormatterTest {
    
    private LocalDateFormatter formatter;
    private String actual, expected;
    private static Date date;
    
    @BeforeClass
    public static void beforeClass() throws ParseException{
        date  = new SimpleDateFormat("dd/mm/yyyy").parse("03/10/2009"); 
    }
    
    @Test
    public void invalidFormat1(){
        try {
            formatter = new LocalDateFormatter("toto", 4, true);
            //L'instruction suivante ne devrait jamais être exécutée car une exception
            //devrait être levée.
            assertTrue("Exception non levée.", false);
        } catch (FormatterInitializationException e) {
            assertEquals("\"toto\" n'est pas un format de date valide.", e.getMessage());
        }
    }
    
    @Test
    public void invalidFormat2() throws FormatterInitializationException {
        try {
            formatter = new LocalDateFormatter("dd/MM/yyyy", 8, true);
            formatter.format(date).toString();
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Le format de date doit correspondre à 8 caractères.", e.getMessage());
        }
    }
    
    @Test
    public void nominal() throws FormatterInitializationException, InvalidSyntaxException{
        formatter = new LocalDateFormatter("dd/mm/yyyy", 10, true);
        actual = formatter.format(date).toString();
        expected = "03/10/2009";
        assertEquals(expected, actual);
    }
    
    @Test
    public void noValueAndNullable() throws InvalidSyntaxException, FormatterInitializationException{
        formatter = new LocalDateFormatter("dd/mm/yyyy", 10, true);
        actual = formatter.format(null).toString();
        expected = "          ";
        assertEquals(expected, actual);
    }
    
    @Test
    public void noValueAndNotNullable() throws InvalidSyntaxException, FormatterInitializationException{
        formatter = new LocalDateFormatter("dd/mm/yyyy", 10, false);
        try{
            formatter.format(null);
            assertTrue("Exception non levée.", false);
        }catch(InvalidSyntaxException e){
            assertEquals("Champ obligatoire.", e.getMessage());
        }
    }

}
