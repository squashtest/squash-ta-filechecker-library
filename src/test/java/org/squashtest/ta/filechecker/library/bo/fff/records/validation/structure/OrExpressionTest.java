/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class OrExpressionTest {

    private static final LitteralExpression EXP1 = new LitteralExpression();
    private static final LitteralExpression EXP2 = new LitteralExpression();
    private static final LitteralExpression EXP3 = new LitteralExpression();
    private static final StringBuilder VAL1 = new StringBuilder("a");
    private static final StringBuilder VAL2 = new StringBuilder("b");
    private static final StringBuilder VAL3 = new StringBuilder("c");
    
    private OrExpression orExp;
    
    static {
        EXP1.setRecordName(VAL1.toString());
        EXP2.setRecordName(VAL2.toString());
        EXP3.setRecordName(VAL3.toString());
    }
    
    @Test
    public void getPattern(){
        expression1();
        String pattern = orExp.getPattern("");
        assertEquals("Le pattern obtenu n'est pas celui attendu.", "(a|b|c)", pattern);
    }
    
    @Test
    public void getOperands(){
        expression3();
        List<String> operands = orExp.getOperands();
        assertEquals("Pas le bon nombre d'opérandes.", 2, operands.size());
        assertEquals("a", operands.get(0));
        assertEquals("b", operands.get(1));
    }
    
    private void expression1(){
        orExp = new OrExpression();
        orExp.addExpression(EXP1);
        orExp.addExpression(EXP2);
        orExp.addExpression(EXP3);
    }
    
    private void expression3(){
        orExp = new OrExpression();
        orExp.addExpression(EXP1);
        orExp.addExpression(EXP2);
        orExp.addExpression(EXP2);
        orExp.addExpression(EXP1);
    }
    
}
