/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import java.text.DecimalFormat;

import org.junit.Test;

import org.squashtest.ta.filechecker.library.bo.fff.formatting.IntegerFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

import static org.junit.Assert.*;

/**
 * @author amd
 */
//Pb: les nombres négatifs n'auront pas le même nombre de caractères que les positifs.
public class IntegerFormatterTest {

    private IntegerFormatter formatter;

    private String actual, expected;

    private Integer myInteger;

    private DecimalFormat decimalFormat;

    @Test
    public void nominal() throws InvalidSyntaxException {
        decimalFormat = new DecimalFormat("00000");
        formatter = new IntegerFormatter(decimalFormat, 5, true);
        myInteger = new Integer(789);
        expected = "00789";
        actual = formatter.format(myInteger).toString();
        assertEquals(expected, actual);
    }

    @Test
    public void invalidFormat() throws InvalidSyntaxException {
        try {
            decimalFormat = new DecimalFormat("toto");
            formatter = new IntegerFormatter(decimalFormat, 9, true);
            myInteger = new Integer(98);
            formatter.format(myInteger);
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Le champ formatté n'a pas le nombre de caractères requis.", e.getMessage());
        }
    }

    @Test
    public void invalidValue() {
        try {
            decimalFormat = new DecimalFormat("000000");
            formatter = new IntegerFormatter(decimalFormat, 5, true);
            myInteger = new Integer(7989);
            formatter.format(myInteger);
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Le champ formatté n'a pas le nombre de caractères requis.", e.getMessage());
        }
    }

    @Test
    public void noValueAndNullable() throws InvalidSyntaxException {
        decimalFormat = new DecimalFormat("00000");
        formatter = new IntegerFormatter(decimalFormat, 9, true);
        expected = "         ";
        actual = formatter.format(null).toString();
        assertEquals(expected, actual);
    }

    @Test
    public void noValueAndNotNullable() {
        try {
            decimalFormat = new DecimalFormat("00000");
            formatter = new IntegerFormatter(decimalFormat, 9, false);
            formatter.format(null);
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Champ obligatoire.", e.getMessage());
        }
    }

}
