/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.utils.comp3;

import org.junit.Test;

import org.squashtest.ta.filechecker.library.utils.mvs.MVSConversion;
import org.squashtest.ta.filechecker.library.utils.mvs.MVSDatatypeException;

import static org.junit.Assert.*;

/**
 * @author amd
 */
public class Comp3ConversionTest {

    @Test
    public void comp3ToString1() {
        // 0000 0001 0010 1100
        byte[] comp3 = new byte[2];
        comp3[0] = 1;
        comp3[1] = 44;
        String actual = MVSConversion.comp3ToString(comp3);
        String expected = "0012";
        assertEquals("Echec de la décompaction. Attendu : " + expected + ", obtenu : " + actual, expected, actual);
    }

    @Test
    public void comp3ToString2() {
        // 0000 0001 0010 1101
        byte[] comp3 = new byte[2];
        comp3[0] = 1;
        comp3[1] = 45;
        String actual = MVSConversion.comp3ToString(comp3);
        String expected = "-012";
        assertEquals("Echec de la décompaction. Attendu : " + expected + ", obtenu : " + actual, expected, actual);
    }

    @Test
    public void comp3ToString3() {
        // 0000 0001 0010 0000
        byte[] comp3 = new byte[2];
        comp3[0] = 1;
        comp3[1] = 32;
        try {
            MVSConversion.comp3ToString(comp3);
            assertTrue("Exception non levée", false);
        } catch (MVSDatatypeException e) {
            assertEquals("Nombre compacte invalide : le dernier demi-octet est inferieur a 9 (1001).", e.getMessage());
        }
    }

    @Test
    public void comp3ToString4() {
        // 0000 0001 1100 1100
        byte[] comp3 = new byte[2];
        comp3[0] = 1;
        comp3[1] = -52; // complément restreint : 1100 1100 --> 1100 1011 -->
        // 0011 0100
        try {
            MVSConversion.comp3ToString(comp3);
            assertTrue("Exception non levée", false);
        } catch (MVSDatatypeException e) {
            assertEquals("Nombre compacte invalide : le 3eme demi-octet est superieur a 9 (1001).", e.getMessage());
        }
    }

    @Test
    public void StringToComp3a() {
        byte[] comp3 = MVSConversion.stringToComp3("12", 5);
        assertEquals("Nombre d'octets du champ compacté incorrect", 5, comp3.length);
        assertEquals(0, comp3[0]);
        assertEquals(0, comp3[1]);
        assertEquals(0, comp3[2]);
        assertEquals(1, comp3[3]);
        assertEquals(44, comp3[4]);
    }

    @Test
    public void StringToComp3b() {
        byte[] comp3 = MVSConversion.stringToComp3("-12", 5);
        assertEquals("Nombre d'octets du champ compacté incorrect", 5, comp3.length);
        assertEquals(0, comp3[0]);
        assertEquals(0, comp3[1]);
        assertEquals(0, comp3[2]);
        assertEquals(1, comp3[3]);
        assertEquals(45, comp3[4]);
    }

    @Test
    public void StringToComp3c() {
        try {
            MVSConversion.stringToComp3("12", 1);
            assertTrue("Exception non levée.", false);
        } catch (MVSDatatypeException e) {
            assertEquals("Le nombre 12 est trop grand pour etre compacte dans un champ de 1 octet(s).", e.getMessage());
        }
    }
    
    @Test
    public void StringToComp3d() {
        try {
            MVSConversion.stringToComp3("-12", 1);
            assertTrue("Exception non levée.", false);
        } catch (MVSDatatypeException e) {
            assertEquals("Le nombre -12 est trop grand pour etre compacte dans un champ de 1 octet(s).", e.getMessage());
        }
    }
    
    @Test
    public void StringToComp3e() {
        try {
            MVSConversion.stringToComp3("12,00", 1);
            assertTrue("Exception non levée.", false);
        } catch (IllegalArgumentException e) {
            assertEquals("12,00 n'est pas un nombre entier.", e.getMessage());
        }
    }

}
