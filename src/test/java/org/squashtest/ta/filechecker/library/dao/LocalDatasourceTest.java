/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.dao;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.filechecker.library.dao.LocalDatasource;

import junit.framework.TestCase;

public class LocalDatasourceTest extends TestCase {
    
    private static String DATASOURCE_DIR = "filechecker/datasource/";
    private LocalDatasource datasource;

    public void testRead1() throws URISyntaxException{
        String sourcePath = "filechecker/datasource/FFF_txt_sample.txt";
        URL url = Thread.currentThread().getContextClassLoader().getResource(sourcePath);
        LocalDatasource datasource = new LocalDatasource(url.toURI());
        try {
            List<String> lines = datasource.read();
            assertEquals(36, lines.size());
            assertTrue(lines.get(0).contains("0000000100018003502400041      0000002316FA 01092008100204"));
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
    
    public void testRead2() throws URISyntaxException{
        String sourcePath = "filechecker/datasource/binary/FFF_binary_sample";
        URL url = Thread.currentThread().getContextClassLoader().getResource(sourcePath);
        LocalDatasource datasource = new LocalDatasource(url.toURI(), true, 65, new StringBuffer("Cp1047"));
        try {
            List<String> lines = datasource.read();
            assertEquals(42, lines.size());
            assertTrue(lines.get(0).startsWith("2008-04-24-22.44.00.403102PT"));
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
    
    public void testWrite() throws URISyntaxException{
        URL url = Thread.currentThread().getContextClassLoader().getResource(DATASOURCE_DIR);
        datasource = new LocalDatasource(new URI(url.getPath() + "testWrite.txt"));
        List<String> lines = new ArrayList<String>();
        lines.add("hello");
        lines.add("world");
        try {
            datasource.write(lines);
            lines = datasource.read();
            assertEquals(2, lines.size());
            assertTrue(lines.get(0).equals("hello"));
            assertTrue(lines.get(1).equals("world"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
       
}
