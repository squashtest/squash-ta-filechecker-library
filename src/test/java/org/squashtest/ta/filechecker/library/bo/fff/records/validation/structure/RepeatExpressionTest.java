/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RepeatExpressionTest {

    private static RepeatExpression repeatExp;
    private static String sExp1 = "exp1";
    
    static {
        repeatExp = new RepeatExpression(2, 3);
        LitteralExpression exp1 = new LitteralExpression();
        exp1.setRecordName(sExp1);
        repeatExp.addExpression(exp1);
    }
    
    @Test
    public void getPattern(){
        String pattern = repeatExp.getPattern("");
        assertEquals("Wrong pattern", "(exp1){2,3}", pattern);
    }
    
}
