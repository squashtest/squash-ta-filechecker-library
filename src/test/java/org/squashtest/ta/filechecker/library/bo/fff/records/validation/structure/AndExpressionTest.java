/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

/**
 * @author amd
 *
 */
public class AndExpressionTest {

    private static final LitteralExpression EXP1;
    private static final LitteralExpression EXP2;
    private static final LitteralExpression EXP3;
    private static final StringBuilder VAL1;
    private static final StringBuilder VAL2;
    private static final StringBuilder VAL3;
    
    private AndExpression andExp;
    
    static{
        VAL1 = new StringBuilder("a");
        VAL2 = new StringBuilder("b");
        VAL3 = new StringBuilder("c");
        EXP1 = new LitteralExpression();
        EXP1.setRecordName(VAL1.toString());
        EXP2 = new LitteralExpression();
        EXP2.setRecordName(VAL2.toString());
        EXP3 = new LitteralExpression();
        EXP3.setRecordName(VAL3.toString());
    }
    
    /**
     * 
     */
    @Test
    public void getPattern(){
        expression1();
        String pattern = andExp.getPattern("");
        assertEquals("Le pattern obtenu n'est pas celui attendu.", "(a&b&c)", pattern);
    }
    
    @Test
    public void getOperands(){
        expression3();
        List<String> operands = andExp.getOperands();
        assertEquals("Pas le bon nombre d'opérandes.", 2, operands.size());
        assertEquals("a", operands.get(0));
        assertEquals("b", operands.get(1));
    }
    
    /**
     * Construit l'expression (a&b&c)
     */
    private void expression1() {
        andExp = new AndExpression();
        andExp.addExpression(EXP1);
        andExp.addExpression(EXP2);
        andExp.addExpression(EXP3);
    }
    
    /**
     * Construit l'expression (a&b&b&a)
     */
    private void expression3() {
        andExp = new AndExpression();
        andExp.addExpression(EXP1);
        andExp.addExpression(EXP2);
        andExp.addExpression(EXP2);
        andExp.addExpression(EXP1);
    }
    
}
