/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.facade;

import static org.junit.Assert.*;

import org.junit.Test;

import org.squashtest.ta.filechecker.library.facade.FileType;

/**
 * @author amd
 *
 */
public class FileTypeTest {

    @Test
    public void validType(){
        FileType type = FileType.getType("SeparatedFieldFile");
        assertEquals(FileType.SeparatedFieldFile, type);
    }

    @Test
    public void InvalidType(){
        try{
            FileType.getType("foo");
            assertTrue("Exception non levée", false);
        }catch(IllegalArgumentException e){
            assertEquals("Le type de fichier \"foo\" n'est pas pris en charge. Les types de fichiers autorisés sont : \"FixedFieldFile\", \"SeparatedFieldFile\".", e.getMessage());            
        }
    }
    
}
