/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author amd
 */
public class RegexpValidatorTest {

    private RegexpValidator validator;

    @Test
    public void nominal() throws InvalidSyntaxException, ValidatorInitializationException {
        validator = new RegexpValidator("\\d{4}", true);
        validator.validate(new StringBuffer("1234"));
    }

    @Test
    public void invalidFormat() {
        try {
            validator = new RegexpValidator("\\d{2{", true);
            assertTrue("Exception non levée.", false);
        } catch (ValidatorInitializationException e) {
            assertEquals("\"\\d{2{\" is not a correct regular expression.", e.getMessage());
        }
    }

    @Test
    public void invalidValue1() throws ValidatorInitializationException {
        validator = new RegexpValidator("\\d{4}", true);
        try {
            validator.validate(new StringBuffer("toto"));
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("\"toto\" doesn't match the format: \"\\d{4}\".", e.getMessage());
        }
    }

    @Test
    public void noValueAndNullable() throws InvalidSyntaxException, ValidatorInitializationException {
        validator = new RegexpValidator("\\d{4}", true);
        validator.validate(new StringBuffer("    "));
    }

    @Test
    public void noDateAndNotNullable() throws ValidatorInitializationException {
        validator = new RegexpValidator("\\d{4}", false);
        try {
            validator.validate(new StringBuffer("    "));
            assertTrue("Exception non levée.", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Champ obligatoire.", e.getMessage());
        }
    }

}
