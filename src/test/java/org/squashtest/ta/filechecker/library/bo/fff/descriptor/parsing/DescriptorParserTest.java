/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parsing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser.FFFdescriptorParser;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.LeafTemplate;
import org.xml.sax.SAXException;

public class DescriptorParserTest {

    private static FFFrecordsTemplate treeModel;
    
    @Before
    public void setUp() throws IOException, SAXException, InvalidDescriptorException{
        URL url = Thread.currentThread().getContextClassLoader().getResource("filechecker/descriptors/FFF_txt_descriptor.xml");
        treeModel = FFFdescriptorParser.getRecordsTemplate(url);
    }
    
    @Test
    public void treeModel(){
        assertFalse(treeModel.isFlat());
        assertTrue(treeModel.isMultiRecords());
        assertEquals("test_file", treeModel.getName());
    }
    
    @Test
    public void test(){
        LeafTemplate leafModel = treeModel.getLeafModel(0);
        assertNotNull(leafModel);
    }
    
}
