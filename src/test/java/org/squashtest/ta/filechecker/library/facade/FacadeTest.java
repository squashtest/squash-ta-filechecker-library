/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.facade;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Test;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.records.builder.InvalidFileException;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.InvalidStructureException;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.xml.sax.SAXException;

/**
 * @author amd
 */
public class FacadeTest {
	
    @Test
    public void validate1() throws FileNotFoundException, IOException, SAXException, URISyntaxException,
                    InvalidFileException, InvalidSyntaxException, InvalidDescriptorException {
        Filechecker facade = new Filechecker();
        URL url = ClassLoader.getSystemResource("filechecker/datasource/FFF_txt_sample.txt");
        URL descriptor = ClassLoader.getSystemResource("filechecker/descriptors/FFF_txt_descriptor.xml");
        facade.loadFile(url.toURI(), descriptor.toURI());
        facade.validateStructureAndSyntax();
    }

    @Test
    public void validate2() throws FileNotFoundException, IOException, SAXException, URISyntaxException,
                    InvalidFileException, InvalidSyntaxException, InvalidDescriptorException {
        Filechecker facade = new Filechecker();
        URL url = Thread.currentThread().getContextClassLoader().getResource("filechecker/datasource/FFF_txt_sample2.txt");
        URL descriptor = ClassLoader.getSystemResource("filechecker/descriptors/FFF_txt_descriptor.xml");
        facade.loadFile(url.toURI(), descriptor.toURI());
        try {
            facade.validateStructureAndSyntax();
            assertTrue("Exception non levée", false);
        } catch (InvalidStructureException e) {
            assertEquals(
                            "Composite \"evenement\", lignes 31 - 36 : ne correspond pas au pattern (101&102&(((103A|103D|103S)&105)){1,8})",
                            e.getMessage());
        }
    }

    @Test
    public void validate3() throws FileNotFoundException, IOException, SAXException, URISyntaxException,
                    InvalidFileException, InvalidSyntaxException, InvalidDescriptorException {
        Filechecker facade = new Filechecker();
        URL url = Thread.currentThread().getContextClassLoader().getResource("filechecker/datasource/FFF_txt_sample3.txt");
        URL descriptor = ClassLoader.getSystemResource("filechecker/descriptors/FFF_txt_descriptor.xml");
        facade.loadFile(url.toURI(), descriptor.toURI());
        try {
            facade.validateStructureAndSyntax();
            assertTrue("Exception non levée", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("Ligne 36, colonnes 1-9 : \"a0000036\" doesn't match the format: \"\\d{8}\".", e.getMessage());
        }
    }

}
