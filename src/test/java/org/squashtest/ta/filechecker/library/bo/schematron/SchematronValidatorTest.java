/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.schematron;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import org.squashtest.ta.filechecker.library.bo.schematron.SchematronValidator;

import static org.junit.Assert.*;

/**
 * @author amd
 */
public class SchematronValidatorTest {

	private SchematronValidator validator = SchematronValidator.getInstance();

	private static final String WRONG_ERR_NB = "The number of errors was smaller or greataer than expected.";

	private List<String> errors;

	private URL schemaUrl;

	@Before
	public void beforeClass() {
		schemaUrl = Thread.currentThread().getContextClassLoader().getResource(
				"schemas/FFFDescriptorSchema_1.1.xsd");
	}

	/**
	 * Test les rÃ¨gles schematron du pattern "names" du schema FixedFieldFileDescriptorSchema_1.0.xsd
	 * 
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void namesTests() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnNamesTests.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 4, errors.size());
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord[2] : Two leaf records may not have the same name.",
				errors.get(0));
		assertEquals(
				
				"/fff:root/fff:records/fff:leaves/fff:leafRecord[3] : A leaf record and a composite record may not have the same name.",
				errors.get(1));
		assertEquals(
				"/fff:root/fff:records/fff:composites/fff:compositeRecord : Two composite records may not have the same name.",
				errors.get(2));
		assertEquals(
				"/fff:root/fff:records/fff:composites/fff:compositeRecord[2] : Opening and closing records must be different.",
				errors.get(3));
	}

	/**
	 * Teste les régles schematron du pattern "fieldAttributes" du schema FixedFieldFileDescriptorSchema_1.0.xsd
	 * 
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void fieldAttributes() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnFieldAttributesTests.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 7, errors.size());
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field : 'Autonumber' field must have a 'sequence' attribute, which value is equal to the 'id' attribute of a 'sequence' element.",
				errors.get(0));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[2] : 'id' and 'constant' fields must have a 'format' attribute, the length of which is equal to the value of the attribute 'length'.",
				errors.get(1));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[3] : 'id' and 'constant' fields must have a 'format' attribute, the length of which is equal to the value of the attribute 'length'.",
				errors.get(2));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[4] : Fields with types 'text', 'date' and 'decimal' must have a 'nullable' attribute.",
				errors.get(3));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[5] : Fields with types 'comp3' must have a 'decimalNb' attribute.",
				errors.get(4));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[5] : Fields with types 'comp3' must have a 'nullable' attribute.",
				errors.get(5));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[5] : The 'comp3', 'signedZoneDecimal' and 'unsignedZoneDecimal' fields could be used only with a binary file.",
				errors.get(6));
	}

	/**
	 * Test les rÃ¨gles schematron du pattern "composites" du schema FixedFieldFileDescriptorSchema_1.0.xsd
	 * 
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void compositesTests() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnCompositesTests.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 4, errors.size());
		assertEquals(
				"/fff:root/fff:records/fff:composites/fff:compositeRecord/fff:openingRecord : Opening record must be defined.",
				errors.get(0));
		assertEquals(
				"/fff:root/fff:records/fff:composites/fff:compositeRecord/fff:closingRecord : Closing record must be defined.",
				errors.get(1));
		assertEquals(
				"/fff:root/fff:records/fff:composites/fff:compositeRecord/fff:children/fff:repeat : 'min' must be smaller than 'max'.",
				errors.get(2));
		assertEquals(
				"/fff:root/fff:records/fff:composites/fff:compositeRecord/fff:children/fff:repeat/fff:record : This record was not defined.", errors.get(3));
	}

	@Test
	public void rootAttributesTest1() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnRootAttributes1.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 1, errors.size());
		assertEquals("/fff:root : Attribute @flat may not be set to 'true' since there's only one type of record.", errors.get(0));
	}

	@Test
	public void rootAttributesTest2() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnRootAttributes2.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 1, errors.size());
		assertEquals("/fff:root : Flat files may not have composite records.", errors.get(0));
	}

	@Test
	public void fields1() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnFields.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 2, errors.size());
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field : Fields must be adjacent.",
				errors.get(0));
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord[2] : Leaf records in multirecord files should have at least one 'id' field.",
				errors.get(1));
	}

	@Test
	public void binaryFile() throws IOException, SAXException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/SchmtrnBinaryFile.xml");
		errors = validator.validate(url, schemaUrl, true);
		assertEquals(WRONG_ERR_NB, 1, errors.size());
		assertEquals(
				"/fff:root/fff:records/fff:leaves/fff:leafRecord/fff:fields/fff:field[2] : Line length must be equal to attribute 'bytesPerLine'.",
				errors.get(0));
	}

}
