/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.facade;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Test;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsTemplate;
import org.xml.sax.SAXException;

/**
 * @author amd
 *
 */
public class TemplateFactoryTest {

    @Test(expected=IllegalArgumentException.class)
    public void fileNotFound() throws IOException, SAXException, InvalidDescriptorException, URISyntaxException  {
        URI descriptor = new URI("filechecker/descriptors/toto.xml");
        TemplateFactory.createTemplate(descriptor);
    }
    
    @Test(expected=InvalidDescriptorException.class)
    public void missingSchema() throws IOException, SAXException, URISyntaxException, InvalidDescriptorException {
    	URL descriptor = ClassLoader.getSystemResource("filechecker/descriptors/MissingSchema.xml");
        TemplateFactory.createTemplate(descriptor.toURI());
    }
    
    @Test
    public void validFFF() throws IOException, SAXException, InvalidDescriptorException, URISyntaxException {
    	URL descriptor = ClassLoader.getSystemResource("filechecker/descriptors/FFF_txt_descriptor.xml");
        AbstractRecordsTemplate template = TemplateFactory.createTemplate(descriptor.toURI());
        assertTrue(template instanceof FFFrecordsTemplate);
    }
    
}
