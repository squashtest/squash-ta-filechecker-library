/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parsing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser.FFFdescriptorParser;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AbstractFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AutonumberField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.ConstantFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.FixedFieldType;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.IdField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.LeafTemplate;
import org.xml.sax.SAXException;

/**
 * @author amd
 * 
 */
public class FieldParsingTest {

	private static LeafRecord leafRecord;

	@BeforeClass
	public static void setUp() throws IOException, SAXException, InvalidDescriptorException {
		URL url = Thread.currentThread().getContextClassLoader().getResource(
				"filechecker/descriptors/FieldParsingTest.xml");
		FFFrecordsTemplate treeModel = FFFdescriptorParser.getRecordsTemplate(url);
		LeafTemplate leafModel = treeModel.getLeafModel(0);
		leafRecord = leafModel.clonePrototype();
	}

	@Test
	public void id() {
		AbstractFixedField field = leafRecord.getField(0);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type IdField.", field.getFieldType().equals(FixedFieldType.id));
		IdField roff = (IdField) field;
		assertEquals("Description du champ", roff.getDescription().toString());
		assertEquals("Libelle du champ", roff.getLabel().toString());
		assertEquals(1, roff.getStart());
		assertEquals(4, roff.getLength());
		assertEquals("toto", roff.getValue().toString());
	}

	@Test
	public void constant() {
		AbstractFixedField field = leafRecord.getField(1);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type ReadOnlyFixedField.", field.getFieldType().equals(FixedFieldType.constant));
		ConstantFixedField roff = (ConstantFixedField) field;
		assertEquals("toto", roff.getValue().toString());
	}

	@Test
	public void unused() {
		AbstractFixedField field = leafRecord.getField(2);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type ReadOnlyFixedField.", field.getFieldType().equals(FixedFieldType.unused));
	}

	@Test
	public void text() {
		AbstractFixedField field = leafRecord.getField(3);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type InputField.", field.getFieldType().equals(FixedFieldType.text));
	}
	@Test
	public void date() {
		AbstractFixedField field = leafRecord.getField(4);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type InputField.", field.getFieldType().equals(FixedFieldType.date));
	}

	@Test
	public void decimal() {
		AbstractFixedField field = leafRecord.getField(5);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type InputField.", field.getFieldType().equals(FixedFieldType.decimal));
	}

	@Test
	public void autonumber() throws Exception {
		AbstractFixedField field = leafRecord.getField(6);
		assertNotNull(field);
		assertTrue("Le champ n'est pas de type AutonumberField.", field.getFieldType().equals(FixedFieldType.autonumber));
		AutonumberField af = (AutonumberField) field;
		assertEquals("MySequence", af.getSequence().getId());
		assertEquals(1, af.getSequence().getStart());
		assertEquals(1, af.getSequence().getIncrement());
	}

}
