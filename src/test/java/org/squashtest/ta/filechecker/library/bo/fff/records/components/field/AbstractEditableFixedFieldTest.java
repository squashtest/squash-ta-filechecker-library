/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.junit.Test;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AbstractVariableFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.InputField;

import static org.junit.Assert.*;

/**
 * @author amd
 *
 */
public class AbstractEditableFixedFieldTest {

    @Test
    public void fillField(){
        AbstractVariableFixedField<String> field = new InputField<String>(null, null, 3, 4, null, null,FixedFieldType.text);
        field.extractValue("1234567890123456789");
        String actual = field.getValue().toString();
        assertEquals("3456", actual);
    }

    
}
