/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AutonumberField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.ValidatorInitializationException;

/**
 * @author amd
 *
 */
public class AutonumberFieldTest {

    private AutonumberField field;
    
    @Before
    public void before() throws ValidatorInitializationException{
        Sequence sequence = new Sequence("nomSequence", 1, 1);
        field = new AutonumberField(null, null, 1, 5, sequence);
    }
    
    @Test
    public void formatAndSetValue() throws InvalidSyntaxException{
        field.setValue("34");
        assertEquals("00034", field.getValue().toString());
    }
    
    @Test
    public void validate() throws InvalidSyntaxException{
        field.setValue("1234a");
        try {
            field.validate();
            assertTrue("Exception non levée", false);
        } catch (InvalidSyntaxException e) {
            assertEquals("colonnes 1-6 : \"1234a\" doesn't match the format: \"\\d{5}\".", e.getMessage());
        }
    }
    
}
