/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import org.junit.Test;

import org.squashtest.ta.filechecker.library.bo.fff.formatting.Padder;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

import static org.junit.Assert.*;

/**
 * @author amd
 *
 */
public class PadderTest {

    private Padder formatter;
    private String expected, actual;
    
    @Test
    public void left() throws InvalidSyntaxException{
        formatter = new Padder('*', true, 10, true);
        expected = "******toto";
        actual = formatter.format("toto").toString();
        assertEquals(expected, actual);
    }
    
    @Test
    public void right() throws InvalidSyntaxException{
        formatter = new Padder('*', false, 10, true);
        expected = "toto******";
        actual = formatter.format("toto").toString();
        assertEquals(expected, actual);
    }
    
    @Test
    public void invalidValue(){
        formatter = new Padder('*', true, 3, true);
        try{
            formatter.format("toto").toString();
            //L'instruction suivante ne devrait jamais être exécutée car une exception
            //devrait être levée.
            assertTrue("Exception non levée.", false);
        }catch(InvalidSyntaxException e){
            assertEquals("Le champ ne doit pas dépasser 3 caractères.", e.getMessage());
        }
    }
    
    @Test
    public void noValueAndNullable() throws InvalidSyntaxException{
        formatter = new Padder('*', true, 10, true);
        expected = "          ";
        actual = formatter.format(null).toString();
        assertEquals(expected, actual);
    }
    
    @Test
    public void noValueAndNotNullable(){
        formatter = new Padder('*', true, 10, false);
        try{
            formatter.format(null).toString();
            assertTrue("Exception non levée.", false);
        }catch(InvalidSyntaxException e){
            assertEquals("Champ obligatoire.", e.getMessage());
        }
    }
    
}