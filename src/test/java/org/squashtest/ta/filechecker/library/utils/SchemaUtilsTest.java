/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.utils;

import java.io.IOException;
import java.net.URL;

import org.junit.Test;
import org.xml.sax.SAXException;

import org.squashtest.ta.filechecker.library.utils.Constants;
import org.squashtest.ta.filechecker.library.utils.SchemaUtils;

import static org.junit.Assert.*;

/**
 * @author amd
 *
 */
public class SchemaUtilsTest {

    @Test
    public void getSchemaName() throws IOException, SAXException{
        URL url = Thread.currentThread().getContextClassLoader().getResource(Constants.DESCRIPTOR_DIR + "FFF_txt_descriptor.xml");
        assertEquals("FFFDescriptorSchema_1.1.xsd", SchemaUtils.getSchemaName(url));
    }
    
    @Test
    public void getSchemaNameWhenSchemaLocationEmpty() throws IOException, SAXException{
        URL url = Thread.currentThread().getContextClassLoader().getResource(Constants.DESCRIPTOR_DIR + "MissingSchema.xml");
        assertNull(SchemaUtils.getSchemaName(url));
    }
    
    @Test
    public void getSchemaNameWhenSchemaLocationPartiallyFilled() throws IOException, SAXException{
        URL url = Thread.currentThread().getContextClassLoader().getResource(Constants.DESCRIPTOR_DIR + "PartialSchemaLocation.xml");
        assertNull(SchemaUtils.getSchemaName(url));
    }
    
}
