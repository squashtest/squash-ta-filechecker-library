/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.builder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser.FFFdescriptorParser;
import org.squashtest.ta.filechecker.library.bo.fff.records.builder.FFFrecordsBuilder;
import org.squashtest.ta.filechecker.library.bo.fff.records.builder.InvalidFileException;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.CompositeRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecord;
import org.squashtest.ta.filechecker.library.bo.iface.Records;
import org.squashtest.ta.filechecker.library.dao.LocalDatasource;

import junit.framework.TestCase;

public class TreeBuilderTest extends TestCase {
    // TODO : modifier pour ne pas dépendre de la création du prototype et de la
    // datasource

    private static String DATASOURCE_DIR = "filechecker/datasource/FFF_txt_sample.txt";

    private Records tree;

    public TreeBuilderTest() throws IOException, SAXException, URISyntaxException, InvalidDescriptorException {
        //datasource
        URL url = Thread.currentThread().getContextClassLoader().getResource(DATASOURCE_DIR);
        LocalDatasource datasource = new LocalDatasource(url.toURI());
        //template
        url = Thread.currentThread().getContextClassLoader().getResource("filechecker/descriptors/FFF_txt_descriptor.xml");
        FFFrecordsTemplate treeModel = FFFdescriptorParser.getRecordsTemplate(url);
        //recordsbuilder
        FFFrecordsBuilder treeBuilder = new FFFrecordsBuilder(datasource, treeModel);
        tree = treeBuilder.buildRecords();
    }

    @Test
    public void test() throws InvalidFileException, InvalidSyntaxException {
        assertEquals(1, tree.size());

        // //0. Etage 0
        assertNotNull(tree);
        assertEquals(1, tree.size());
        AbstractRecord root = tree.get(0);
        assertTrue(root instanceof CompositeRecord);
        CompositeRecord composite = (CompositeRecord ) root;
        assertEquals("Nom de la racine", "fichier", composite.getName().toString());
        LeafRecord opening = composite.getOpeningRecord();
        assertNotNull(opening);
        assertEquals("Opening name", "000", opening.getName().toString());
        LeafRecord closing = composite.getClosingRecord();
        // assertNotNull(closing);
        // assertEquals("Closing name", "999", closing.getName().toString());
        List<AbstractRecord> children = composite.getChildren();
        assertEquals("Nombre d'enfants", 4, children.size());

        // 1. Etage 1
        AbstractRecord child = children.get(0);
        assertTrue(child instanceof CompositeRecord);
        assertEquals("Nom enfant 0", "lot", child.getName().toString());
        composite = (CompositeRecord ) child;
        opening = composite.getOpeningRecord();
        assertNotNull(opening);
        assertEquals("Nom opening", "010", opening.getName().toString());
        closing = composite.getClosingRecord();
        assertNotNull(closing);
        assertEquals("Nom closing", "099", closing.getName().toString());
        children = composite.getChildren();
        assertEquals("Nombre d'enfants", 1, children.size());

        // 2. Etage 2
        child = children.get(0);
        assertTrue(child instanceof CompositeRecord);
        assertEquals("Nom enfant 0", "evenement", child.getName().toString());
        composite = (CompositeRecord ) child;
        opening = composite.getOpeningRecord();
        assertNotNull(opening);
        assertEquals("Nom opening", "100", opening.getName().toString());
        closing = composite.getClosingRecord();
        assertNull(closing);
        children = composite.getChildren();
        assertEquals("Nombre d'enfants", 4, children.size());

        // 2. Etage 2
        child = children.get(0);
        assertTrue(child instanceof LeafRecord);
        assertEquals("Nom enfant 0", "101", child.getName().toString());
        child = children.get(1);
        assertTrue(child instanceof LeafRecord);
        assertEquals("Nom enfant 1", "102", child.getName().toString());
        child = children.get(2);
        assertTrue(child instanceof LeafRecord);
        assertEquals("Nom enfant 2", "103D", child.getName().toString());
        child = children.get(3);
        assertTrue(child instanceof LeafRecord);
        assertEquals("Nom enfant 3", "105", child.getName().toString());

        for (AbstractRecord rec : tree) {
            rec.validate();
        }
    }

}
