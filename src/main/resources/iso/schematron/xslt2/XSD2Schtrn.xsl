<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2019 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<!--
        based on an original transform by Eddie Robertsson
        2001/04/21      fn: added support for included schemas
        2001/06/27      er: changed XMl Schema prefix from xsd: to xs: and changed to the Rec namespace
        
        2009/09/14      amd: changed schematron schema URI from "http://www.ascc.net/xml/schematron" to "http://purl.oclc.org/dsdl/schematron"
        2009/10/16      amd: changed XSLT version from "1.0" to "2.0"
-->
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <!-- Set the output to be XML with an XML declaration and use indentation -->
        <xsl:output method="xml" omit-xml-declaration="no" indent="yes" standalone="yes"/>
        <!-- -->
        <!-- match schema and call recursive template to extract included schemas -->
        <!-- -->
        <xsl:template match="xs:schema">
                <!-- call the schema definition template ... -->
                <xsl:call-template name="gatherSchema">
                        <!-- ... with current current root as the $schemas parameter ... -->
                        <xsl:with-param name="schemas" select="/"/>
                        <!-- ... and any includes in the $include parameter -->
                        <xsl:with-param name="includes" 
						select="document(/xs:schema/xs:*[self::xs:include or self::xs:import or self::xs:redefine]/@schemaLocation)"/>
                </xsl:call-template>
        </xsl:template>
        <!-- -->
        <!-- gather all included schemas into a single parameter variable -->
        <!-- -->
        <xsl:template name="gatherSchema">
                <xsl:param name="schemas"/>
                <xsl:param name="includes"/>
                <xsl:choose>
                        <xsl:when test="count($schemas) &lt; count($schemas | $includes)">
                                <!-- when $includes includes something new, recurse ... -->
                                <xsl:call-template name="gatherSchema">
                                        <!-- ... with current $includes added to the $schemas parameter ... -->
                                        <xsl:with-param name="schemas" select="$schemas | $includes"/>
                                        <!-- ... and any *new* includes in the $include parameter -->
                                        <xsl:with-param name="includes" 
										select="document($includes/xs:schema/xs:*[self::xs:include or self::xs:import or self::xs:redefine]/@schemaLocation)"/>
                                </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                                <!-- we have the complete set of included schemas, 
								so now let's output the embedded schematron-->
                                <xsl:call-template name="output">
                                        <xsl:with-param name="schemas" select="$schemas"/>
                                </xsl:call-template>
                        </xsl:otherwise>
                </xsl:choose>
        </xsl:template>
        <!-- -->
        <!-- output the schematron information -->
        <!-- -->
        <xsl:template name="output">
                <xsl:param name="schemas"/>
                <!-- -->
                <sch:schema>
                        <!-- get header-type elements - eg title and especially ns -->
                        <!-- title (just one) -->
                        <xsl:copy-of select="$schemas//xs:appinfo/sch:title[1]"/>
                        <!-- get remaining schematron schema children -->
                        <!-- get non-blank namespace elements, dropping duplicates -->
                        <xsl:for-each select="$schemas//xs:appinfo/sch:ns">
                                <xsl:if test="generate-id(.) = 
								generate-id($schemas//xs:appinfo/sch:ns[@prefix = current()/@prefix][1])">
                                        <xsl:copy-of select="."/>
                                </xsl:if>
                        </xsl:for-each>
                        <xsl:copy-of select="$schemas//xs:appinfo/sch:phase"/>
                        <xsl:copy-of select="$schemas//xs:appinfo/sch:pattern"/>
                        <sch:diagnostics>
                                <xsl:copy-of select="$schemas//xs:appinfo/sch:diagnostics/*"/>
                        </sch:diagnostics>
                </sch:schema>
        </xsl:template>
        <!-- -->
</xsl:transform>