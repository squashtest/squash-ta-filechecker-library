/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface IDatasource {

    /**
     * Ecrit une collection de lignes dans un fichier.
     * Si le fichier n'existe pas préalablement, 
     * il sera créé  
     * @param lines = lignes à écrire dans le fichier
     * @throws IOException if the named file 
     * exists but is a directory rather than a 
     * regular file, does not exist but cannot 
     * be created, or cannot be opened for any 
     * other reason<br>
     * - if an I/O error occurs*/
    public void write(List<String> lines) throws IOException;
    
    /**
     * Lit un fichier séquentiel
     * @return une collection de chaînes de caractères correspondant 
     * aux lignes du fichier séquentiel lu.
     * @throws IOException si une IOException survient lors de la 
     * lecture d'une ligne
     * @throws FileNotFoundException si le fichier n'existe pas,
     * est un répertoire, ou ne peut être lu pour quelqu'autre 
     * raison que ce soit.
     */
    public List<String> read() throws IOException, FileNotFoundException;
    
}
