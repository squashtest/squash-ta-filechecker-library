/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.squashtest.ta.filechecker.library.bo.fff.formatting.IFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.IValidator;
import org.squashtest.ta.filechecker.library.bo.iface.UserInput;

/**
 * Représente un champ éditable d'un enregistrement à champs fixes.
 * 
 * @author amd
 */
public class InputField<E> extends AbstractVariableFixedField<E> implements UserInput {

	private FixedFieldType type;
	
    /**
     * @author amd
     *
     */

    //Pour création et clonage
    public InputField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, IFormatter<E> pFormatter, IValidator pValidator, FixedFieldType pType) {
        label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        formatter = pFormatter;
        validator = pValidator;
        type= pType;
    }
   
    // On fait une copie de surface pour tous les attributs, sauf value.
    // (design pattern poids-mouche).
    @Override
    public Object clone() {
        return new InputField<E>(label, description, start, length, formatter, validator, type);
    }

	@Override
	public FixedFieldType getFieldType() {
		return type;
	}
    
    
    
}
