/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.CompositeRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure.AbstractExpression;
import org.squashtest.ta.filechecker.library.bo.fff.template.CompositeTemplate;

public class CompositeModelFactory {

	// ---------------------------------------------
	private StringBuffer name;

	private StringBuffer label;

	private StringBuffer desc;

	private AbstractExpression childrenPattern;

	private String openingName;

	private String closingName;

	private List<String> descendants;

	public static final Logger LOGGER = LoggerFactory.getLogger(CompositeModelFactory.class);

	// ---------------------------------------------
	// PARSER

	public void setName(String pName) {
		name = new StringBuffer(pName);
	}

	public void setLabel(String pLabel) {
		label = new StringBuffer(pLabel);
	}

	public void setDesc(String pDesc) {
		desc = new StringBuffer(pDesc);
	}

	public void setOpeningName(String pOpeningName) {
		openingName = pOpeningName;
	}

	public String getClosingName() {
		return closingName;
	}

	public void setClosingName(String pClosingName) {
		closingName = pClosingName;
	}

	public void addExpression(AbstractExpression pChildrenPattern) {
		childrenPattern = pChildrenPattern;
	}

	// ---------------------------------------------
	// TREEMODELFACTORY

	void setDescendants(List<String> pDescendants) {
		descendants = pDescendants;
	}

	AbstractExpression getChildrenPattern() {
		return childrenPattern;
	}

	String getOpeningName() {
		return openingName;
	}

	String getName() {
		return name.toString();
	}

	CompositeTemplate createCompositeModel() {
		CompositeRecord prototype = new CompositeRecord(name, label, desc, childrenPattern);
		return new CompositeTemplate(openingName, closingName, descendants, prototype);
	}

}
