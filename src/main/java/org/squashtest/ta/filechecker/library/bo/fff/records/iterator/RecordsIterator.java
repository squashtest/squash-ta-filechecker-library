/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.CompositeRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecord;

/**
 * @author Agnes
 *
 */
//----------------------------------------------------------------
// CLASSE INTERNE : ITERATEUR SUR LES ELEMENTS DE RECORDS
//----------------------------------------------------------------
/**
 * Cf. composite pattern.
 * <p>
 * Iterateur externe permettant d'itérer sur une composition d'objets de type
 * <code>Record</code>.
 * 
 * @author amd
 */
public class RecordsIterator implements Iterator<AbstractRecord> {

    // -------------------------------------------------------------------------
    // Déclarations
    // -------------------------------------------------------------------------
    // Pile (LIFO) d'itérateurs, permettant d'itérer sur tous les
    // éléments de la composition.
    /** */
    private Stack<Iterator<AbstractRecord>> stack;

    // -------------------------------------------------------------------------
    // Constructeur
    // -------------------------------------------------------------------------
    public RecordsIterator(Iterator<AbstractRecord> pIterator) {
        stack = new Stack<Iterator<AbstractRecord>>();
        stack.push(pIterator);
    }

    // -------------------------------------------------------------------------
    // Implémentations des méthodes abstraites de l'interface Iterator
    // -------------------------------------------------------------------------
    /**
     * Implémentation de la méthode abstraite <code>next</code> de l'interface
     * {@link java.util.Iterator Iterator}.
     * 
     * @return <code>true</code> si l'itération a un élément suivant,
     *         <code>false</code> sinon.
     */
    // TODO : javadoc : comment récupérer la javadoc de l'interface
    public boolean hasNext() {
        Iterator<AbstractRecord> iterator;
        // Si la pile est vide, il n'y a pas d'élément suivant,
        // donc on retourne "false".
        if (stack.empty()) {
            return false;
            // Si la pile n'est pas vide :
            // On regarde si l'itérateur situé au sommet de la pile
            // a un élément suivant.
            // --> S'il n'a pas d'élément suivant : on le retire de la pile
            // et on rappelle la méthode hasNext() de manière récursive.
            // --> S'il a un élément suivant : on retourne "true".
        } else {
            // NB : la méthode peek() retourne l'objet situé au sommet
            // de la pile sans le retirer de la pile
            iterator = (Iterator<AbstractRecord> ) stack.peek();
            if (!iterator.hasNext()) {
                // NB : la méthode pop() retourne l'objet situé au sommet
                // de la pile et le retire de la pile
                stack.pop();
                // appel récursif de la méthode hasNext()
                return hasNext();
            } else {
                return true;
            }
        }
    }

    /**
     * Implémentation de la méthode abstraite <code>next</code> de l'interface
     * {@link java.util.Iterator Iterator}.
     * 
     * @return l'élément suivant dans l'itération.
     * @throws NoSuchElementException si l'itération n'a plus d'élément.
     **/
    public AbstractRecord next() {
        Iterator<AbstractRecord> iterator;
        AbstractRecord record = null;
        // on récupère l'élément suivant de l'itérateur courant (situé au sommet
        // de la pile)
        if (hasNext()) {
            iterator = (Iterator<AbstractRecord> ) stack.peek();
            record = iterator.next();
            // Si cet élément est un élément composé,
            // il faut itérer sur ses enfants.
            // A cette fin, on ajoute son itérateur au sommet de la
            // pile.
            if (record instanceof CompositeRecord) {
                // la méthode push(element) ajoute un élément au sommet de la
                // pile
                CompositeRecord composite = (CompositeRecord ) record;
                LeafRecord closing = composite.getClosingRecord();
                if (closing!=null){
                    stack.push(new SingleIterator(closing));
                }
                stack.push(composite.getChildren().iterator());
                stack.push(new SingleIterator(composite.getOpeningRecord()));
            }
            return record;
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * @throws UnsupportedOperationException
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }
    
    class SingleIterator implements Iterator<AbstractRecord> {

        private LeafRecord leaf;

        private boolean hasNext = true;

        SingleIterator(LeafRecord pLeaf) {
            leaf = pLeaf;
        }

        /*
         * (non-Javadoc)
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext() {
            return hasNext;
        }

        /*
         * (non-Javadoc)
         * @see java.util.Iterator#next()
         */
        public AbstractRecord next() {
            if (hasNext) {
                hasNext = false;
                return leaf;
            } else {
                throw new IllegalStateException();
            }
        }

        /*
         * (non-Javadoc)
         * @see java.util.Iterator#remove()
         */
        public void remove() {
            // TODO Auto-generated method stub

        }

    }

}


