/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax;

import java.text.DecimalFormat;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.DecimalFormatter;

public class DoubleValidator implements IValidator {

    public static final Logger LOGGER = LoggerFactory.getLogger(DoubleValidator.class);
    private Pattern pattern;
    private final boolean nullable;
    private final String format;
    private DecimalFormat decimalFormat;

    public DoubleValidator(String pFormat, boolean pNullable) throws ValidatorInitializationException {
        try {
            pattern = Pattern.compile(pFormat);
        } catch (IllegalArgumentException e) {
            throw new ValidatorInitializationException(e, "\"{}\" is not a correct regular expression.", pFormat);
        }
        nullable = pNullable;
        format = pFormat;
    }

    /* (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.bo.fff.validation.syntax.IValidator#validate()
     */
    public void validate(StringBuffer pValue) throws InvalidSyntaxException {
        String value = pValue.toString();
        //accept the txt input decimal with "," as separator but then replace it by "."
        String valueFormalized=value.replace(",", ".");
        if (StringUtils.isWhitespace(value)) {
            if (!nullable) {
                throw new InvalidSyntaxException("Champ obligatoire.");
            }
        } else {
            try {
                //create the DecimalFormatter with the input pattern from xml file
                decimalFormat = new DecimalFormat(pattern.pattern());

                //create a double with the input value from txt file
                //the Double.parseDouble() method will remove all unnecessary 0 from the string
                //for exmaple: -00000789.600000 -> -789.6 
                Double myDouble = Double.parseDouble(valueFormalized);

                //create a formated string from the double with the DecimalFormatter above
                String actual;

                //assert if the 2 strings: origin value and formatted actual have the same content
                actual = new DecimalFormatter(decimalFormat, pattern.pattern().length(), true).format(myDouble).toString();
                //in case of the Local setting for Separator after the action format is ",", replace them by "." 
                actual = actual.replaceAll(",", ".");

                if (valueFormalized.startsWith("-")) {
                    valueFormalized = valueFormalized.replaceAll("-", "-0");
                }
                if (!valueFormalized.equals(actual)) {
                    throw new InvalidSyntaxException("\"{}\" doesn't match the format: \"{}\".", value, format);
                }

            } catch (NumberFormatException e) {
                throw new InvalidSyntaxException("{} is not a decimal number.", pValue);
            }
        }

    }
}
