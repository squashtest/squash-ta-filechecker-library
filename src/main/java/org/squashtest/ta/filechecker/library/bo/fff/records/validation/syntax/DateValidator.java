/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;


/**
 * @author amd
 *
 */
public class DateValidator implements IValidator {

    private SimpleDateFormat dateFormat;
    private String format;
    private boolean nullable;
    
    public DateValidator(String pFormat, boolean pNullable) throws ValidatorInitializationException  {
        try{
            dateFormat = new SimpleDateFormat(pFormat);
        }catch(IllegalArgumentException e){
            throw new ValidatorInitializationException(e, "\"{}\" is not a valid date format.", pFormat);
        }
        format = pFormat;
        nullable = pNullable;
    }
    
    /* (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.bo.fff.validation.syntax.IValidator#validate()
     */
    public void validate(StringBuffer pValue) throws InvalidSyntaxException {
        String value = pValue.toString();
        if (StringUtils.isWhitespace(value)){
            if (!nullable){
                throw new InvalidSyntaxException("Champ obligatoire.");
            }
        }else{
            try {
                dateFormat.parse(value);
            } catch (ParseException e) {
                throw new InvalidSyntaxException(e, "\"{}\" isn't a date to the format: \"{}\".", value, format);
            }
        }
    }

}
