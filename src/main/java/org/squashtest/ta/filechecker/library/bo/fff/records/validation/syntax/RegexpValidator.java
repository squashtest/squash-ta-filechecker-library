/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author amd
 *
 */
public class RegexpValidator implements IValidator {

    public static final Logger LOGGER = LoggerFactory.getLogger(RegexpValidator.class);
    private Pattern pattern;
    private boolean nullable;
    private String format;
    
    public RegexpValidator(String pFormat, boolean pNullable) throws ValidatorInitializationException {
        try{
            pattern = Pattern.compile(pFormat);
        }catch (IllegalArgumentException e){
            throw new ValidatorInitializationException(e, "\"{}\" is not a correct regular expression.", pFormat);
        }
        nullable = pNullable;
        format = pFormat;
    }

    /* (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.bo.fff.validation.syntax.IValidator#validate()
     */
    public void validate(StringBuffer pValue) throws InvalidSyntaxException {
        String value = pValue.toString();
        if (StringUtils.isWhitespace(value)){
            if (!nullable){
                throw new InvalidSyntaxException("Champ obligatoire.");
            }
        }else{
                Matcher matcher = pattern.matcher(value);
                if (!matcher.matches()){
                throw new InvalidSyntaxException("\"{}\" doesn't match the format: \"{}\".", value, format);
            }
        }
    }

}
