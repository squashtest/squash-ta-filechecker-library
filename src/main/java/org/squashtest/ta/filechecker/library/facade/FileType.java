/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.facade;


/**
 * @author Agnes
 *
 */
public enum FileType {

    FixedFieldFile, SeparatedFieldFile;

    /**
     * @param pType
     * @return
     */
    public static FileType getType(String pType) {
        FileType type = null;
        boolean notFound = true;
        FileType[] fileTypes = values();
        int size = fileTypes.length;
        int index = 0;
        while(notFound && index < size){
            FileType fileType = fileTypes[index++];
            if (fileType.toString().equals(pType)){
                notFound = false;
                type = fileType;
            }
        }
        
        if (notFound) {
            StringBuilder buf = new StringBuilder();

            buf.append("Le type de fichier \"");
            buf.append(pType);
            buf.append("\" n'est pas pris en charge. Les types de fichiers autorisés sont : \"");
            buf.append(fileTypes[0].toString());
            for (int ind = 1; ind < size; ind++) {
                buf.append("\", \"");
                buf.append(fileTypes[ind].toString());
            }
            buf.append("\".");
            throw new IllegalArgumentException(buf.toString());
        }
        
        return type;
    }

}

