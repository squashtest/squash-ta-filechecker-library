/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.AlwaysValidValidator;

public class UnusedField extends AbstractFixedField {

    public UnusedField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength) {
		//NB : la validation schematron garantit que la valeur a une longueur égale à length
		label = pLabel;
		description = pDescription;
		start = pStart;
		length = pLength;
		validator = new AlwaysValidValidator();
	}
	
	@Override
	public FixedFieldType getFieldType() {
		return FixedFieldType.unused;
	}

	@Override
	public Object clone() {
		return new UnusedField(label, description, start, length);
	}

}
