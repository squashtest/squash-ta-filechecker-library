/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.utils;



import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SchemaValidationErrorHandler implements ErrorHandler {

	private List<String> exceptionsList;
	
	public SchemaValidationErrorHandler()
	{
		exceptionsList = new ArrayList<String>();
	}
	
	@Override
	public void warning(SAXParseException exception) throws SAXException {
		StringBuilder builder = new StringBuilder("WARNING - ");
		addException(builder, exception);
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		StringBuilder builder = new StringBuilder("ERROR - ");
		addException(builder, exception);
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		throw new SAXException(exception);
	}
	
	private void addException(StringBuilder builder,SAXParseException exception)
	{
		
		builder.append("Schema/XSD validation - Line: ");
		builder.append(exception.getLineNumber());
		builder.append(" - ");
		builder.append(exception.getLocalizedMessage());
		exceptionsList.add(builder.toString());
	}
	
	public List<String> getExceptions(){
		return exceptionsList;
	}

}
