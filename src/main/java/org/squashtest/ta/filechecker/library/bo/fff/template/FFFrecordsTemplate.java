/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.template;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsTemplate;
import org.squashtest.ta.filechecker.library.facade.FileType;

public class FFFrecordsTemplate extends AbstractRecordsTemplate{
	
    private boolean flat;
    
    private boolean multiRecords;
    
    // Templates
    private List<LeafTemplate> leafModels;

    /**
     * <ul>
     *   <li>clé = nom de l'enregistrement ouvrant du composite</li>
     *   <li>valeur = modèle de composite</li
     * </ul>
     */
    private Map<String, CompositeTemplate> compositeModels;
    
    /**
     * @param pName
     * @param pFlat
     * @param pMultiRecords
     * @param pLeafModels
     * @param pCompositeModels
     */
    public FFFrecordsTemplate(String pName, boolean pIsBinary, int pBytesPerRecord, StringBuffer pEncoding, boolean pFlat, boolean pMultiRecords, List<LeafTemplate> pLeafModels,
                    Map<String, CompositeTemplate> pCompositeModels) {
        isBinary = pIsBinary;
        bytesPerRecord = pBytesPerRecord;
        encoding = pEncoding;
        name = pName;
        flat = pFlat;
        multiRecords = pMultiRecords;
        leafModels = pLeafModels;
        compositeModels = pCompositeModels;
    }

    //pour treebuilder
    public boolean isFlat() {
        return flat;
    }

    //pour treebuilder
    public boolean isMultiRecords() {
        return multiRecords;
    }

    //pour treebuilder
    public CompositeTemplate getCompositeModel(String pOpeningName) {
        return compositeModels.get(pOpeningName);
    }

    //pour treebuilder
    /**
     * Compare la ligne <code>pLine</code> avec les enregistrements feuilles du
     * <code>Prototype</code> de ce <code>TreeBuilder</code>, afin d'identifier
     * l'enregistrement auquel la ligne correspond.
     * 
     * @param pLine Ligne à analyser
     * @param lineNumber Numèro de la ligne à analyser
     * @return L'enregistrement prototype auquel correspondant la ligne analysée
     * @throws UnknownRecordException Si la ligne ne correspond à aucun
     *             enregistrement connu.
     */
    // XXX gérer le cas où il y a plusieurs enregistrements possibles? (si
    // identifiants mal définis donc non discriminant)
    // XXX faire un passage sur n° de ligne
    public LeafTemplate identifyLeafModel(final String pLine, int lineNumber) throws UnknownRecordException {
        Iterator<LeafTemplate> leafModelIterator = leafModels.iterator();
        LeafTemplate leafModel = null;
        boolean nomatch = true;
        while (nomatch && leafModelIterator.hasNext()) {
            leafModel = leafModelIterator.next();
            nomatch = !leafModel.matches(pLine);
        }
        if (nomatch) { // la ligne n'a pas pu être identifiée
        	StringBuilder msg = new StringBuilder("La ligne n°{} ne correspond à aucun des enregistrements possibles.");
        	if(isBinary)
        	{
        		msg.append("(champ(s) de type \"id\" incorrects)");
        	}else{
        		msg.append("(longueur et/ou champ(s) de type \"id\" incorrects)");
        	}
            throw new UnknownRecordException(msg.toString(), lineNumber);
        }
        return leafModel;
    }

    //pour treebuilder (monorecord)
    public LeafTemplate getLeafModel(final int pIndex) {
        return leafModels.get(pIndex);
    }
    
    public String getName(){
        return name;
    }
    
    public boolean isBinary() {
        return isBinary;
    }

    public StringBuffer getEncoding() {
        return encoding;
    }

    public int getBytesPerRecord() {
        return bytesPerRecord;
    }

    @Override
    public FileType getType() {
        return FileType.FixedFieldFile;
    }
    
    

}
