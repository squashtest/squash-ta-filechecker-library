/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.template;

import org.slf4j.helpers.MessageFormatter;

/**
 * Exception levée lorsqu'il est impossible de créer/utiliser un pool de connexion à une
 * BDD du projet.
 * 
 * @author amd
 */
@SuppressWarnings("serial")
public class UnknownRecordException extends RuntimeException {

    /**
     * Crée une nouvelle instance de {@link UnknownRecordException}
     */
    public UnknownRecordException() {
        super();
    }

    /**
     * Crée une nouvelle instance de {@link UnknownRecordException}
     * 
     * @param pCause Exception parente de cette exception.
     */
    public UnknownRecordException(final Throwable pCause) {
        super(pCause);
    }

    /**
     * Crée une nouvelle instance de {@link UnknownRecordException}
     * 
     * @param pMsg Message d'erreur paramétrable (chaque paramètre est remplacé
     *            par {}).
     * @param pParams Paramètres du message d'erreur, s'il y en a.
     */
    public UnknownRecordException(final String pMsg, final Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage());
    }

    /**
     * Crée une nouvelle instance de {@link UnknownRecordException}
     * 
     * @param pCause Exception parente de cette exception.
     * @param pMsg Message d'erreur paramétrable (chaque paramètre est remplacé
     *            par {}).
     * @param pParams Paramètres du message d'erreur, s'il y en a.
     */
    public UnknownRecordException(final Throwable pCause, final String pMsg, final Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage(), pCause);
    }

}
