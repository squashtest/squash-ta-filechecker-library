/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;
import org.apache.commons.digester.AbstractObjectCreationFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;


/** Permet au parser xml de créer une instance de la classe Sequence en 
 * en passant au constructeur les attributs de l'élément [sequence]*/
public class SequenceFactory extends AbstractObjectCreationFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(SequenceFactory.class);
    
    /** */
    @Override
    public Object createObject(Attributes pAttributes) throws Exception {
        //La validation xsd garantit que id, start et increment sont renseignés
        String id = pAttributes.getValue("id");
        int start = Integer.parseInt(pAttributes.getValue("start"));
        int increment = Integer.parseInt(pAttributes.getValue("increment"));
        Sequence seq = new Sequence(id, start, increment);
        if (LOGGER.isDebugEnabled()){
            Object[] args = {id, start, increment};
            LOGGER.debug("Création de la séquence \"{}\" (start = {}, increment = {})", args);
        }
        return seq;
    }

}
