/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.xml;
///**
// * 
// */
//package org.squashtest.ta.filechecker.bo.xml;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Set;
//import java.util.Map.Entry;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathConstants;
//import javax.xml.xpath.XPathExpression;
//import javax.xml.xpath.XPathExpressionException;
//import javax.xml.xpath.XPathFactory;
//
//import org.w3c.dom.Document;
//import org.xml.sax.SAXException;
//
//import org.squashtest.ta.filechecker.bo.fff.template.InvalidFileException;
//import org.squashtest.ta.filechecker.utils.ConfigurationException;
//import org.squashtest.ta.filechecker.utils.Constants;
//import org.squashtest.ta.filechecker.utils.xpath.XpathAssertions;
//
///**
// * @author amd
// */
//@SuppressWarnings("serial")
//public class XmlXpathValidation extends HashMap<String, Map<String, String>> {
//
//	public static String validate(String pFileName, String pXpathQueriesFile) throws ParserConfigurationException,
//			IOException {
//
//		// 1. Document xml à vérifier
//		Document doc;
//		try {
//			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
//			domFactory.setNamespaceAware(true);
//			DocumentBuilder builder = domFactory.newDocumentBuilder();
//			InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
//					Constants.DOWNLOAD_DIR + pFileName);
//			if (stream == null)
//				throw new IOException("Le fichier \"" + pFileName + "\" est introuvable.");
//			doc = builder.parse(stream);
//		} catch (SAXException e) {
//			throw new InvalidFileException("Le fichier {} est incorrect.", pFileName);
//		} catch (IOException e) {
//			throw new IOException("Erreur lors de l'accès au fichier \"" + pFileName + "\".");
//		}
//
//		// 2. Assertions xpath à exécuter
//		XpathAssertions assertions;
//		try {
//			assertions = new XpathAssertions(pXpathQueriesFile);
//		} catch (SAXException e) {
//			throw new ConfigurationException(e, "Le fichier {} est incorrect.", pXpathQueriesFile);
//		}
//
//		// 3. Exécution des assertions
//		String query = null;
//		boolean success = true;
//		Set<Entry<String, Integer>> set = assertions.entrySet();
//		for (Entry<String, Integer> entry : set) {
//			XPath xpath = XPathFactory.newInstance().newXPath();
//			XPathExpression expr;
//			try {
//				query = entry.getKey();
//				expr = xpath.compile(query);
//				Object obj = expr.evaluate(doc, XPathConstants.BOOLEAN);
//				success &= ((Boolean) obj).booleanValue();
//				if (!success) {
//					return entry.getValue();
//				}
//			} catch (XPathExpressionException e) {
//				throw new ConfigurationException(e, "La requête {} est incorrecte.", query);
//			} catch (ClassCastException e) {
//				throw new ConfigurationException("La requête {} doit renvoyer vrai ou faux.", query);
//			}
//		}
//		return "success";
//
//	}
//
//}
