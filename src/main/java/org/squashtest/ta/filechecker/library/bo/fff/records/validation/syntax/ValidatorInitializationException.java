/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax;

import org.slf4j.helpers.MessageFormatter;

/**
 * 
 * Manage exception occurred during Validator initialization.
 *
 */
@SuppressWarnings("serial")
public class ValidatorInitializationException extends Exception {

	/**
     * Constructor
     */
    public ValidatorInitializationException() {
        super();
    }

    /**
     * Constructor
     * 
     * @param pCause Exception parent exception.
     */
    public ValidatorInitializationException(Throwable pCause) {
        super(pCause);
    }

    /**
     * Constructor
     * 
     * @param pMsg Parameterizable error message (Each parameter is replace by {}).
     * @param pParams Array of error message parameter (If there is)
     */
    public ValidatorInitializationException(String pMsg, Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage());
    }

    /**
     * Constructor
     * 
     * @param pCause Parent exception
     * @param pMsg Parameterizable error message (Each parameter is replace by {}).
     * @param pParams Array of error message parameter (If there is)
     */
    public ValidatorInitializationException(Throwable pCause, String pMsg, Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage(), pCause);
    }
	
}
