/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AbstractFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AutonumberField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.FixedFieldType;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.IdField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;
import org.squashtest.ta.filechecker.library.bo.fff.template.LeafTemplate;

/**
 * Permet d'effectuer le mapping entre les éléments leafRecord du fichier xml de
 * description et les objets {@link LeafRecord} et {@link LeafTemplate}
 * 
 * @author amd
 */
public class LeafModelFactory {

    // -----------------------------------------------------------------------
    private static final Logger LOGGER = LoggerFactory.getLogger(LeafModelFactory.class);
    private StringBuffer name;

    private StringBuffer label;

    private StringBuffer description;

    private List<String> sequencesToReset;

    private List<FieldFactory> fieldFactories;

    private TreeModelFactory treeModelFactory;
    
    private Factory factory;

    // -----------------------------------------------------------------------
    // PARSER :
    
    public LeafModelFactory() {
        sequencesToReset = new ArrayList<String>();
        fieldFactories = new ArrayList<FieldFactory>();
        factory = new Factory();
    }
    
    public void setName(String pName){
        name = new StringBuffer(pName);
    }

    public void setLabel(String pLabel) {
        label = (pLabel==null)?null:new StringBuffer(pLabel);
    }

    public void setDescription(String pDescription) {
        description = (pDescription==null)?null:new StringBuffer(pDescription);
    }

    public void addSequenceToReset(String pSequenceId) {
        sequencesToReset.add(pSequenceId);
    }

    public void addFieldFactory(FieldFactory pFieldFactory) {
        fieldFactories.add(pFieldFactory);
    }

    public void setTreeModelFactory(TreeModelFactory pTreeModelfactory) {
        treeModelFactory = pTreeModelfactory;
    }
    
    String getName() {
        return name.toString();
    }

    // -----------------------------------------------------------------------
    //TREEMODELFACTORY

    public LeafTemplate createLeafModel() throws InvalidDescriptorException {
        return factory.createLeafModel();
    }

    // -----------------------------------------------------------------------
    
    // NB : classe interne pour lisibilité code 
    private class Factory {

        private List<AbstractFixedField> fields;

        private List<AutonumberField> fieldsToIncrement;

        private List<IdField> ids;

        private int length = 0;

        Factory() {
            fields = new ArrayList<AbstractFixedField>();
            fieldsToIncrement = new ArrayList<AutonumberField>();
            ids = new ArrayList<IdField>();
        }
        
        LeafTemplate createLeafModel() throws InvalidDescriptorException {
            
            // Objets 'fields', 'fieldsToIncrement', 'sequencesToReset', 'ids' et 'length'
            for (FieldFactory fieldFactory : fieldFactories){
                AbstractFixedField field = fieldFactory.createField();
                fields.add(field);
                FixedFieldType fieldType = field.getFieldType(); 
                if (fieldType.equals(FixedFieldType.autonumber)) {
                    fieldsToIncrement.add((AutonumberField ) field);
                } else if (fieldType.equals(FixedFieldType.id)) {
                    ids.add((IdField)field);
                }
                length += field.getLength();
            }
            
            // Objet 'sequencesToReset'
            List<Sequence> seqToReset = new ArrayList<Sequence>();
            for (String seqName : sequencesToReset){
                Sequence seq = treeModelFactory.getSequence(seqName);
                seqToReset.add(seq);
            }
            
            // Création de l'objet LeafRecord
            
            LeafRecord leaf =
                            new LeafRecord(name, label, description, fields, fieldsToIncrement, seqToReset);
            
            // Création de l'objet LeafModel
            return new LeafTemplate(leaf, ids, length);
        }

    }



}
