/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence;

/**
 * Séquence d'auto-incrémentation
 * */
public class Sequence {

    private String id;
    private int start;
    private int increment;
    private int curVal;
    
    /**Crée une nouvelle séquence
     * @param pId identifiant de la séquence
     * @param pStart valeur de départ de la séquence
     * @param pIncrement pas d'incrémentation de la séquence */
    public Sequence(String pId, int pStart, int pIncrement) {
        id = pId;
        start = pStart;
        increment = pIncrement;
        curVal = pStart;
    }

    /** @return l'identifiant de la séquence */
    public String getId(){
        return id;
    }

    /**@return la valeur de départ de la séquence*/
    public int getStart() {
        return start;
    }

    /** @return le pas d'incrémentation de la séquence */
    public int getIncrement() {
        return increment;
    }
    
    /** @return la valeur en cours et incrémente cette séquence */
    public int getCurVal() {
        int current = curVal;
        curVal+=increment;
        return current;
    }

    /** Réinitialise la séquence (valeur courante {@literal <--} valeur de départ)*/
    public void reset(){
        curVal=start;
    }
    
}