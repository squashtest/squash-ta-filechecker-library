/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.utils.xpath;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.digester.Digester;
import org.apache.commons.lang.StringUtils;
import org.squashtest.ta.filechecker.library.utils.Constants;
import org.squashtest.ta.filechecker.library.utils.SchemaUtils;
import org.squashtest.ta.filechecker.library.utils.SchemaValidationErrorHandler;
import org.xml.sax.SAXException;

/**
 * @author amd
 */
//@SuppressWarnings("serial")
public class XpathAssertions {

	//extends ArrayList<XpathAssertion>
	
    private URL url;

    private List<XpathAssertion> assertionList;
    /*public XpathAssertions(String pFileName) throws IOException, SAXException, InvalidXpathQueriesException {
        url = Thread.currentThread().getContextClassLoader().getResource(Constants.XPATH_QUERIES_DIR + pFileName);
        if (url == null)
            throw new FileNotFoundException("Fichier " + pFileName + " introuvable.");
        validate();
        setAssertions();
    }*/
    
    /*public XpathAssertions(){
    }*/

    /**
     * @param xpathQueriesFileUrl
     * @throws SAXException 
     * @throws IOException 
     * @throws InvalidXpathQueriesException 
     */
    public XpathAssertions(URL xpathQueriesFileUrl) throws IOException, SAXException, InvalidXpathQueriesException {
        url = xpathQueriesFileUrl;
        assertionList = new ArrayList<XpathAssertion>();
        validate();
        setAssertions();
    }



	public void add(String queryName, String query, Integer result){
    	assertionList.add(new XpathAssertion(queryName, query, result));
    }

    private void setAssertions() throws IOException, SAXException {
        Digester d = new Digester();
        d.setValidating(false);
        d.setNamespaceAware(true);
        Class<?>[] classTypes = {String.class, String.class, Integer.class};
        d.addCallMethod("root/assertion", "add", 3, classTypes);
        d.addCallParam("root/assertion/queryName", 0);
        d.addCallParam("root/assertion/query", 1);
        d.addCallParam("root/assertion/result", 2);
        d.push(this);
        d.parse(url);
    }
    
    private void validate() throws IOException, SAXException, InvalidXpathQueriesException {
    	SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    	String schemaName = SchemaUtils.getSchemaName(url);
    	if(StringUtils.isBlank(schemaName))
    	{
    		throw new InvalidXpathQueriesException("{} should reference a FFFQuerySchema schema to be a valid FFF Queries file",url.toString());
    	}
    	URL schemaUrl = Thread.currentThread().getContextClassLoader().getResource(Constants.SCHEMA_DIR+schemaName);
        Schema schema = schemaFactory.newSchema(schemaUrl);
        Validator validator = schema.newValidator();
        Source source = new StreamSource(new File(url.getPath()));
        SchemaValidationErrorHandler errHandler = new SchemaValidationErrorHandler();
        validator.setErrorHandler(errHandler);
        try {
			validator.validate(source);
			List<String> errorList = errHandler.getExceptions();
			errorManager(errorList);
		} catch (SAXException e) {
			throw new InvalidXpathQueriesException(e, "{} is not a valid FFF Queries file: "+e.getMessage(), url);
		}
	}
    
    private void errorManager(List<String> errorList)throws InvalidXpathQueriesException{
    	if (errorList.size() > 0) {
	    	StringBuilder builder = new StringBuilder(url.toString());
			builder.append(" is not a valid FFF queries file");
			builder.append(Constants.LINE_SEPARATOR);
			Iterator<String> errorIt = errorList.iterator();
			while (errorIt.hasNext()) {
				String error = errorIt.next();
				builder.append(error);
				if(errorIt.hasNext())
				{
					builder.append(Constants.LINE_SEPARATOR);
				}
			}
			throw new InvalidXpathQueriesException(builder.toString());
    	}
    }

	public Iterator<XpathAssertion> iterator() {
		return assertionList.iterator();
	}
}

