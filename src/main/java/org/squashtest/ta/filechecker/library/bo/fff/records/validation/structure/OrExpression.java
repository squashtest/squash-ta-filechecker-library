/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrExpression extends AbstractExpression {

    private List<AbstractExpression> expressions;

    public static final Logger LOGGER = LoggerFactory.getLogger(OrExpression.class);

    // pour parser
    public OrExpression() {
        expressions = new ArrayList<AbstractExpression>();
    }

    // pour parser
    public void addExpression(AbstractExpression pExp) {
        expressions.add(pExp);
    }

    @Override
    public String getPattern( String separator ) {
        StringBuilder pattern = new StringBuilder("(");
        int max = expressions.size() - 1;
        int index = 0;
        while (index < max) {
            pattern.append(expressions.get(index).getPattern(separator));
            pattern.append("|");
            index++;
        }
        pattern.append(expressions.get(index).getPattern(separator));
        pattern.append(")");
        return pattern.toString();
    }
    
    public List<String> getOperands() {
        
        //NB : la validation xsd garantit qu'il y a au min 2 expressions
        int expNb = expressions.size();
        int index = 1;
        List<String> liste1 = expressions.get(0).getOperands();
        List<String> liste2;
        
        while (index<expNb){
            
            liste2 = expressions.get(index++).getOperands();
            for (String operand : liste2) {
                boolean notFound = true;
                int opNb = liste1.size();
                int index2 = 0;
                while (notFound && index2<opNb){
                    if (operand.equals(liste1.get(index2++))){
                        notFound = false;
                    }
                }
                if (notFound){
                    liste1.add(operand);
                }
            }
        }
        return liste1;
    }

}
