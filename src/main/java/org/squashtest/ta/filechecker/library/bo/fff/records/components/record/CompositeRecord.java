/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.record;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure.AbstractExpression;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecord;
import org.squashtest.ta.filechecker.library.utils.Constants;

/**
 * Cf. composite pattern Représente un enregistrement du fichier, lui-même
 * constitué d'enregistrements enfants
 * 
 * @author amd
 */
public class CompositeRecord extends AbstractRecord {

    private LeafRecord openingRecord;

    private LeafRecord closingRecord;

    private List<AbstractRecord> childrenList = new ArrayList<AbstractRecord>();

    private AbstractExpression childrenPattern;

    public static final Logger LOGGER = LoggerFactory.getLogger(CompositeRecord.class);

    // ----------------------------------------------------------------
    // CREATION INITIALE ET CLONAGE DU PROTOTYPE
    // ----------------------------------------------------------------

    /**
     * Crée une nouvelle instance de <code>CompositeRecord</code>.
     * 
     * @param pName Nom de cet enregistrement
     * @param pLabel Libellé de cet enregistrement
     * @param pDescription Description de cet enregistrement
     * @param pChildrenPattern Arbre syntaxique représentant les séquences
     *            possibles d'enregistrements enfants de cet enregistrement
     *            (attribut <code>children</code>)
     */
    public CompositeRecord(StringBuffer pName, StringBuffer pLabel, StringBuffer pDescription,
                    AbstractExpression pChildrenPattern) {
        super(pName, pLabel, pDescription);
        childrenPattern = pChildrenPattern;
    }

    // -----------------------------------------------------------------
    // CLONAGE DU PROTOTYPE
    // -----------------------------------------------------------------

    /**
     * Clonage de cet enregistrement (Cf. design pattern Prototype).
     * <ul>
     * <li>Les attributs <code>name</code>, <code>label</code>,
     * <code>description</code> et <code>childrenPattern</code> sont codés en surface
     * (Cf. design pattern flyweight).</li>
     * <li>les attributs <code>openingRecord</code>, <code>closingRecord</code>
     * et <code>children</code> ne sont pas clonés.</li>
     * </ul>
     * 
     * @return une copie de cet enregistrement.
     */
    public Object clone() {
        return new CompositeRecord(name, label, description, childrenPattern);
    }

    // -----------------------------------------------------------------
    // AUTOINCREMENTATION (POUR CREATION FICHIER)
    // -----------------------------------------------------------------
    public void autoincrement() throws InvalidSyntaxException {
        for (AbstractRecord child : childrenList) {
            child.autoincrement();
        }
    }

    // -----------------------------------------------------------------
    // VALIDATION STRUCTURELLE ET SYNTAXIQUE
    // -----------------------------------------------------------------
    /**
     * Vérifie que l'enregistrement est valide
     * 
     * @throws InvalidSyntaxException si l'enregistrement viole les règles syntaxiques et
     *             que le projet est en mode "exception".
     * @throws InvalidStructureException si l'enregistrement n'est pas correctement structuré et
     *             que le projet est en mode "exception".
     */
    @Override
    public void validate() throws InvalidSyntaxException, InvalidStructureException {
        
        //XXX si pas d'enfant?
        
        // STRUCTURE
        LOGGER.debug("Validation de la structure du composite \"{}\".", name);
        
        String separator ="@";
        
        String builder = childrenAsString(separator);
        String patternStr = fullChildrenPattern(separator);
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(builder);
        boolean isValid = matcher.find();
        if (!isValid){
            String msg = "Composite \"{}\", lignes {} - {} : ne correspond pas au pattern {}";
            throw new InvalidStructureException(msg, name, getFirstLineNb(), getLastLineNb(), childrenPattern.getPattern(""));
        }
        
        // Validation de la syntaxe
        openingRecord.validate();
        for (AbstractRecord child : childrenList) {
            child.validate();
        }
        if (closingRecord!=null){
            closingRecord.validate();
        }
        
    }
    
    private int getFirstLineNb(){
        return openingRecord.getLineNb();
    }
    
    private int getLastLineNb(){
        int last = -1;
        if (closingRecord!=null){
            last = closingRecord.getLineNb();
        }else {
            int childrenNb = childrenList.size();
            if (childrenNb>0){ //pas de closing, des enfants
                AbstractRecord lastChild = childrenList.get(childrenNb-1);
                if (lastChild instanceof LeafRecord){
                    last = ((LeafRecord ) lastChild).getLineNb();
                }else{
                    last = ((CompositeRecord ) lastChild).getLastLineNb();
                }
            }else{ //pas de closing, pas d'enfant
                last = openingRecord.getLineNb();
            }
        }
        return last;
    }

    // -----------------------------------------------------------------
    // SAUVEGARDE DU FICHIER
    // -----------------------------------------------------------------

    // Surcharge de la méthode "toString()" de la superclasse "Object"
    // On appelle la méthode "toString()" de chacun des sous-éléments
    // de l'instance, et on concatène leur résultat à une chaîne
    // de caractères.
    // Si, lors de l'itération, on rencontre un élément composé,
    // sa méthode "toString()" lancera une nouvelle itération, etc.
    // XXX javadoc de la classe mère
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (openingRecord != null) {
            buf.append(openingRecord.toString());
        }
        buf.append(Constants.LINE_SEPARATOR);
        Iterator<AbstractRecord> iterator = childrenList.iterator();
        while (iterator.hasNext()) {
            buf.append(iterator.next().toString());
        }
        if (closingRecord != null) {
            buf.append(closingRecord.toString());
        }
        return buf.toString();
    }

    // -----------------------------------------------------------------
    // CONSTRUCTION DE L'ARBRE
    // -----------------------------------------------------------------
    // pour construction de l'arbre
    /**
     * @param pOpeningRecord L'enregistrement ouvrant (attribut
     *            <code>openingRecord</code>) de cet enregistrement.
     */
    public void setOpeningRecord(LeafRecord pOpeningRecord) {
        openingRecord = pOpeningRecord;
    }

    // pour clonage et TreeBuilder
    /**
     * @param pClosingRecord L'enregistrement fermant (attribut
     *            <code>fermantRecord</code>) de cet enregistrement.
     */
    public void setClosingRecord(LeafRecord pClosingRecord) {
        closingRecord = pClosingRecord;
    }

    public void addChild(AbstractRecord child) {
        childrenList.add(child);
    }

    public LeafRecord getOpeningRecord() {
        return openingRecord;
    }

    public LeafRecord getClosingRecord() {
        return closingRecord;
    }

    public List<AbstractRecord> getChildren() {
        return childrenList;
    }
    
    private String childrenAsString( String separator){
    	
        StringBuilder builder = new StringBuilder();
        for (AbstractRecord record : childrenList) {
        	builder.append(separator);
        	builder.append(record.getName());
        	builder.append(separator);
		}
    	return builder.toString();
    }
    
    private String fullChildrenPattern( String separator){
    	StringBuilder patternBuilder = new StringBuilder();
        patternBuilder.append("^");
        patternBuilder.append(childrenPattern.getPattern(separator));
        patternBuilder.append("$");
        return patternBuilder.toString();
    }
    
}
