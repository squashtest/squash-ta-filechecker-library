/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.builder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.CompositeRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.fff.template.CompositeTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.LeafTemplate;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecord;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsBuilder;
import org.squashtest.ta.filechecker.library.bo.iface.Records;
import org.squashtest.ta.filechecker.library.dao.IDatasource;

/**
 * Mappe les lignes du fichier séquentiel avec les enregistrements objets
 * correspondant.
 * 
 * @author amd
 */
public class FFFrecordsBuilder extends AbstractRecordsBuilder {

    private List<String> lines;

    private FFFrecordsTemplate treeModel;

    private static final Logger LOGGER = LoggerFactory.getLogger(FFFrecordsBuilder.class);

    // ----------------------------------------------------------------------
    // Constructeur
    // ----------------------------------------------------------------------
    /**
     * Constructeur.
     * 
     * @param datasource objet qui implémente les opérations de
     *            lecture/écriture du fichier séquentiel traité
     * @param pTreeModel prototype du fichier séquentiel
     */
    public FFFrecordsBuilder(IDatasource datasource, FFFrecordsTemplate pTreeModel) throws FileNotFoundException,
                    IOException {
        lines = datasource.read();
        treeModel = pTreeModel;
    }

    @Override
    public Records buildRecords() {
        Records records = new Records();
        boolean multiRecords = treeModel.isMultiRecords();
        if (multiRecords) {
            boolean flat = treeModel.isFlat();
            List<AbstractRecord> flatTree = buildFlatTree();
            if (flat) {
                records.addAll(flatTree);
            } else {
                records.addAll(buildTallTree(flatTree));
            }
        } else {
            records.addAll(buildMonoRecordTree());
        }
        return records;
    }

    // ----------------------------------------------------------------------
    // FICHIER AVEC UN SEUL TYPE D'ENREGISTREMENT
    // ----------------------------------------------------------------------
    private List<AbstractRecord> buildMonoRecordTree() {
        List<AbstractRecord> flatTree = new ArrayList<AbstractRecord>();
        int count = lines.size();
        int lineNumber = 0;
        LeafTemplate leafModel = treeModel.getLeafModel(0);
        int length = leafModel.getLength();
        while (lineNumber < count) {
            String line = lines.get(lineNumber++);
            if (line.length() < length) {
                throw new InvalidFileException("La ligne \"{}\" est trop courte.", line);
            }
            LeafRecord newRecord = leafModel.clonePrototype();
            try {
                newRecord.fillFields(line);
            } catch (IllegalArgumentException e) {
                // NB : lineNumber a deja ete incremente --> permet de commencer a 1 et non 0
                throw new IllegalArgumentException("Ligne " + lineNumber + " " + e.getMessage() , e);
            }
            flatTree.add(newRecord);
        }
        return flatTree;
    }

    // --------------------------------------------------------------------------
    // FICHIER AVEC PLUSIEURS TYPES D'ENREGISTREMENTS
    // (--> chaque enregistrement doit avoir un ou plusieurs champs
    // identifiants)
    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------
    // Construction d'un arbre plat à plusieurs types de champs
    // --------------------------------------------------------------------------
    /**
     * @param pInvalidLines <code>List</code> dans laquelle seront répertoriés
     *            les numéros des lignes (comptées à partir de 1) qui n'ont pas
     *            été reconnues.
     * @return Une <code>List</code> d'enregistrements feuilles formés d'après
     *         les lignes <code>lines</code> de ce <code>TreeBuilder</code>.
     */
    private List<AbstractRecord> buildFlatTree() {
        List<AbstractRecord> flatTree = new ArrayList<AbstractRecord>();
        int count = lines.size();
        int lineNumber = 0;
        LeafTemplate leafModel;
        while (lineNumber < count) {
            String line = lines.get(lineNumber++);
            leafModel = treeModel.identifyLeafModel(line, lineNumber);
            LeafRecord newRecord = leafModel.clonePrototype();
            LOGGER.debug("Ligne \"{}\" : enregistrement \"{}\"", lineNumber, newRecord.getName());
            newRecord.fillFields(line);
            newRecord.setLineNb(lineNumber);
            flatTree.add(newRecord);
        }
        return flatTree;
    }

    // ----------------------------------------------------------------------
    // Construction d'un arbre non plat à plusieurs types de champs
    // ----------------------------------------------------------------------

    /**
     * @param pFlatTree
     * @return
     */
    private List<AbstractRecord> buildTallTree(List<AbstractRecord> pFlatTree) {
        TallTreeBuilder ctb = new TallTreeBuilder(pFlatTree, pFlatTree.size(), 0);
        return ctb.buildCompositeTree();
    }

    // ----------------------------------------------------------------------
    // Inner class compositeTreeBuilder
    // ----------------------------------------------------------------------

    private class TallTreeBuilder {

        private List<AbstractRecord> flatTree;

        private int leavesAmount;

        private int leafNb;

        private TallTreeBuilder(final List<AbstractRecord> pFlatTree, final int pLeavesAmount, final int pLeafNb) {
            flatTree = pFlatTree;
            leavesAmount = pLeavesAmount;
            leafNb = pLeafNb;
        }

        private List<AbstractRecord> buildCompositeTree() {
            List<AbstractRecord> tree = new ArrayList<AbstractRecord>();
            while (leafNb < leavesAmount) {
                AbstractRecord record = buildRecord();
                tree.add(record);
                LOGGER.debug("Ajout à l'arbre de l'enregistrement \"{}\"", record.getName());
            }
            return tree;
        }

        private AbstractRecord buildRecord() {
            AbstractRecord record;
            LeafRecord leaf = (LeafRecord ) flatTree.get(leafNb);
            leafNb++;
            CompositeTemplate compModel = treeModel.getCompositeModel(leaf.getName().toString());
            // 1. leaf est un enregistrement feuille simple
            if (compModel == null) {
                record = leaf;
            } else {
                record = buildComposite(compModel, leaf);
            }
            return record;
        }

        private AbstractRecord buildComposite(final CompositeTemplate pCompModel, final LeafRecord pLeaf) {

            final CompositeRecord compRecord = pCompModel.getCompositeWithOpening(pLeaf);
            LOGGER.debug("Construction du composite \"{}\" avec l'ouvrant \"{}\"", compRecord.getName(), compRecord
                            .getOpeningRecord().getName());
            final String closingName = pCompModel.getClosingName();

            // compRecord n'a pas d'enregistrement fermant
            if (closingName == null) {
                final List<String> descendants = pCompModel.getDescendants();
                fillComposite(compRecord, descendants);
                // compRecord a un enregistrement fermant
            } else {
                fillComposite(compRecord, closingName);
            }

            return compRecord;
        }

        private void fillComposite(CompositeRecord pCompRecord, String pClosingName) {

            boolean incomplete = true;
            while (incomplete && leafNb < leavesAmount) {

                LeafRecord leaf = (LeafRecord ) flatTree.get(leafNb);

                // 1. leaf est l'enregistrement fermant de pCompRecord
                if (leaf.getName().toString().equals(pClosingName)) {
                    pCompRecord.setClosingRecord(leaf);
                    leafNb++;
                    incomplete = false;
                    LOGGER.debug("Ajout de l'enregistrement fermant \"{}\" au composite {}", pClosingName, pCompRecord
                                    .getName());
                    // 2. leaf est un enregistrement enfant de pCompRecord
                } else {
                    AbstractRecord record = buildRecord();
                    pCompRecord.addChild(record);
                    LOGGER.debug("Ajout de l'enregistrement enfant \"{}\" au composite {}", record.getName(),
                                    pCompRecord.getName());
                }
            }
        }

        private void fillComposite(CompositeRecord pCompRecord, List<String> pDescendants) {
            boolean incomplete = true;

            while (incomplete && leafNb < leavesAmount) {

                // NB : On n'incrémente pas leafNb car on n'est pas sûr de
                // consommer
                // leaf dans cette méthode.
                LeafRecord leaf = (LeafRecord ) flatTree.get(leafNb);
                String leafName = leaf.getName().toString();

                // On boucle sur les descendants directs de compRecord pour voir
                // si leaf en fait partie
                int descAmount = pDescendants.size();
                int descNb = 0;
                boolean notFound = true;

                while (notFound && descNb < descAmount) {
                    String descendant = pDescendants.get(descNb++);
                    if (leafName.equals(descendant)) {
                        notFound = false;
                    }
                }

                // 1. Si leaf n'est pas un descendant de compRecord
                if (notFound) {
                    incomplete = false;
                    // 2. Si leaf est un descendant de compRecord
                } else {
                    // On incrémente leafNb puisque leaf est consommé ici
                    pCompRecord.addChild(buildRecord());
                    LOGGER.debug("Ajout de l'enregistrement enfant \"{}\" au composite \"{}\"", leafName, pCompRecord
                                    .getName());
                }
            }
        }
    }

}
