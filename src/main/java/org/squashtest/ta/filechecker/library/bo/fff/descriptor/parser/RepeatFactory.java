/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;

import org.apache.commons.digester.AbstractObjectCreationFactory;
import org.xml.sax.Attributes;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure.RepeatExpression;

public class RepeatFactory extends AbstractObjectCreationFactory {

    @Override
    public Object createObject(Attributes pAttributes) throws Exception {
        // La validation par le schéma xsd/schematron garantit que la valeur de
        // 'min' est un entier non négatif, que la valeur de 'max' est un entier
        // positif ou 'unbounded', et que min<max
        int min = Integer.parseInt(pAttributes.getValue("min"));
        String sMax = pAttributes.getValue("max");
        int max;
        if (sMax.equals("unbounded")) {
            max = Integer.MAX_VALUE;
        } else {
            max = Integer.parseInt(sMax);
        }
        return new RepeatExpression(min, max);
    }

}
