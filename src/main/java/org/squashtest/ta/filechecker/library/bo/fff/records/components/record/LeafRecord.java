/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.record;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AbstractFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AutonumberField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecord;
import org.squashtest.ta.filechecker.library.utils.Constants;

/**
 * Représente un enregistrement simple (ne contenant pas lui-même
 * d'enregistrement) du fichier.
 * 
 * @author amd
 */
public class LeafRecord extends AbstractRecord {

    /** Liste des champs composant l'enregistrement. */
    private List<AbstractFixedField> fields;

    /**
     * Map répertoriant les champs à auto-incrémenter.<br>
     * --> clé = champ à incrémenter<br>
     * --> valeur = nom de la séquence à utiliser pour l'incrémentation<br>
     */
    private List<AutonumberField> fieldsToIncrement;

    /**
     * Liste répertoriant les séquences qui doivent être réinitialisées à partir
     * de ce type d'enregistrement
     */
    private List<Sequence> sequencesToReset;

    private int lineNb;

    public static final Logger LOGGER = LoggerFactory.getLogger(LeafRecord.class);

    // ----------------------------------------------------------------
    // CREATION INITIALE DU PROTOTYPE
    // ----------------------------------------------------------------

    /**
     * @param pName
     * @param pLabel
     * @param pDescription
     * @param pFields
     * @param pFieldsToIncrement
     * @param pSequencesToReset
     */
    public LeafRecord(StringBuffer pName, StringBuffer pLabel, StringBuffer pDescription, List<AbstractFixedField> pFields,
                    List<AutonumberField> pFieldsToIncrement, List<Sequence> pSequencesToReset) {
        super(pName, pLabel, pDescription);
        this.fields = pFields;
        this.fieldsToIncrement = pFieldsToIncrement;
        this.sequencesToReset = pSequencesToReset;
    }

    // -----------------------------------------------------------------
    // CLONAGE DU PROTOTYPE
    // -----------------------------------------------------------------

    @Override
    public Object clone() {
        LeafRecord clone = new LeafRecord(name, label, description, sequencesToReset);
        for (AbstractFixedField field : fields) {
            AbstractFixedField clonedField = (AbstractFixedField ) field.clone();
            clone.addField(clonedField);
            // se chargera aussi des fieldsToIncrement
        }
        return clone;
    }

    /**
     * Crée une nouvelle instance de <code>LeafRecord</code>.
     * 
     * @param pName Nom de cet enregistrement
     * @param pLabel Libellé de cet enregistrement
     * @param pDescription Description de cet enregistrement
     * @param pSequencesToReset Sequences qui doivent être réinitialisées à
     *            partir de cet enregistrement
     */
    private LeafRecord(StringBuffer pName, StringBuffer pLabel, StringBuffer pDescription, List<Sequence> pSequencesToReset) {
        super(pName, pLabel, pDescription);
        sequencesToReset = pSequencesToReset;
        fields = new ArrayList<AbstractFixedField>();
        fieldsToIncrement = new ArrayList<AutonumberField>();
    }

    // pour le clonage
    /**
     * Ajoute un champ à l'enregistrement.
     * 
     * @param pField champ à ajouter
     */
    public void addField(AbstractFixedField pField) {
        fields.add(pField);
        if (pField instanceof AutonumberField) {
            fieldsToIncrement.add((AutonumberField ) pField);
        }
    }

    // -----------------------------------------------------------------
    // CONSTRUCTION DE L'ARBRE
    // -----------------------------------------------------------------

    // pour construction de l'arbre à partir lecture fichier
    public void fillFields(String pLine) {
        for (AbstractFixedField field : fields) {
            field.extractValue(pLine);
        }
    }

    // -----------------------------------------------------------------
    // AUTOINCREMENTATION (POUR CREATION FICHIER)
    // -----------------------------------------------------------------

    /**
     * - Réinitialise les séquences qui doivent redémarrer à partir de cet
     * enregistrement. - Incrémente les champs auto-incrémentés de
     * l'enregistrement (ex. : N° de ligne)
     * 
     * @throws InvalidSyntaxException
     */
    public void autoincrement() throws InvalidSyntaxException {
        for (Sequence seq : sequencesToReset) {
            seq.reset();
        }
        for (AutonumberField auto : fieldsToIncrement) {
            auto.increment();
        }
    }

    // -----------------------------------------------------------------
    // VALIDATION SYNTAXIQUE
    // -----------------------------------------------------------------

    // TODO modifier!!!
    @Override
    public void validate() throws InvalidSyntaxException {
        LOGGER.debug("Validation de la syntaxe de l'enregistrement feuille \"{}\".", name);
        for (AbstractFixedField field : fields) {
            try {
                field.validate();
            } catch (InvalidSyntaxException e) {
                throw new InvalidSyntaxException(e,"Ligne {}, {}", lineNb, e.getMessage());
            }
        }
    }

    // -----------------------------------------------------------------
    // VALIDATION DES CHAMPS AUTOINCREMENTES
    // -----------------------------------------------------------------

    // -----------------------------------------------------------------
    // SAUVEGARDE DU FICHIER
    // -----------------------------------------------------------------

    // Surcharge de la méthode "toString()" de la superclasse "Object"
    // Pour sauvergarde du fichier
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        for (AbstractFixedField field : fields) {
            buf.append(field.toString());
        }
        buf.append(Constants.LINE_SEPARATOR);
        return buf.toString();
    }

    public AbstractFixedField getField(final int pIndex) {
        return fields.get(pIndex);
    }

    public List<Sequence> getSequencesToReset() {
        return sequencesToReset;
    }

    public List<AutonumberField> getFieldsToIncrement() {
        return fieldsToIncrement;
    }

    public int getLineNb() {
        return lineNb;
    }

    public void setLineNb(int pLineNb) {
        lineNb = pLineNb;
    }

    public List<AbstractFixedField> getFields() {
        return fields;
    }

}
