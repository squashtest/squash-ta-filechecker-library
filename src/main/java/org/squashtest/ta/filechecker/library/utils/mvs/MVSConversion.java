/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.utils.mvs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class to handle IBM Z/Series packed decimal data in Java. All packed
 * number operands must be valid format with valid sign. The longest packed
 * decimal number handled is 31 digits or 16 bytes. Java long will not hold the
 * maximum packed decimal number. java.math.bigdecimal can be used to handle
 * larger numbers than will fit in java long. "Decimal packed" format is a
 * method of representing numbers in the IBM Z/series computers. It was also
 * present in the 360, 370 and 390 series.
 * <p>
 * Decimal digits are stored as 4 bits, 0 through 9, two digits per byte.<br>
 * The last four bits of a number are reserved for a sign. Positive are binary
 * 1010, 1100 1110 and 1111. Negative is 1011 and 1101. For example the number
 * -354 woud be stored as 0x354d, 7251 would be stored 0x07251c.
 * <p>
 * COBOL is a popular mainframe language that has a number format with a "USAGE"
 * of COMPUTATIONAL-3 or CCOMP-3. COMP-3 is stored as packed decimal. An
 * example: <pre><code>
 *      01 PART-NUMBER.
 *      05  PART-NAME                   PIC X(20).
 *      05  PART-NUMBER                 PIC 9(5) USAGE IS COMP-3.
 *      05  PART-COST                   PIC 9(5)V99 USAGE IS COMP-3.
 *      05  FILLER                      PIC X(10).
 * </code></pre>
 * <p>The PART-NUMBER would be stored in memory as packed decimal and
 * occupy 3 bytes. The PART-COST would use 4 bytes. The implied decimal does not
 * reserve any storage</p><p>Z/Series, 360, 370 and 390 is a trademark of IBM
 * Corporation</p>
 * 
 * @see <a href="http://www.ibm.com">IBM.COM</a>
 * @see <a href="http://benjaminjwhite.name/zdecimal">Z Decimal for java project home page</a>
 * @author zdecimal [ at ] benjaminjwhite.name
 * @version 3.0
 */

public class MVSConversion {

    static final char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    private static final String INVALID_DIGIT_NIBBLE =
                    "Nombre compacte invalide : le {}eme demi-octet est superieur a 9 (1001).";

    private static final String INVALID_SIGN_NIBBLE =
                    "Nombre compacte invalide : le dernier demi-octet est inferieur a 9 (1001).";

    private static final String NUMBER_TOO_BIG =
                    "Le nombre {} est trop grand pour etre compacte dans un champ de {} octet(s).";

    /**
     * Convertit un tableau d'octets contenant un nombre compacte (comp-3) en
     * une chaine de caracteres contenant le nombre decompacte.
     * 
     * @param pComp3Nb Tableau d'octets contenant un nombre compacte (comp-3)
     * @return Chaine de caracteres contenant le nombre decompacte
     * @exception MVSDatatypeException Si le table d'octet passe en argument n'est pas
     *                un nombre au format comp-3
     */
    public static String comp3ToString(byte[] pComp3Nb) throws MVSDatatypeException {
        int len = pComp3Nb.length;
        StringBuilder outStrng = new StringBuilder(2 * len);
        int nibble; // un demi-octet (chaque chiffre est code sur 4 bits. Les
        // derniers 4 bits correspondent au signe)

        // 1. On boucle sur tous les octets, sauf le dernier (qui contient le
        // signe)
        int byteIndex;
        for (byteIndex = 0; byteIndex < len - 1; byteIndex++) {
            // a. Demi-octet gauche
            // On veut la valeur du premier demi-octet : on decale les bits
            // quatre fois vers la droite, en remplacant les bits manquant a
            // gauche par des zeros.
            nibble = (pComp3Nb[byteIndex] & 0xf0) >>> 4;
            if (nibble > 9)
                throw new MVSDatatypeException(INVALID_DIGIT_NIBBLE, byteIndex * 2 + 1);
            outStrng.append(digits[nibble]);
            // b. Deuxieme demi-octet
            // On veut la valeur du deuxieme demi-octet : on remplace le premier
            // demi-octet par des zeros.
            nibble = (pComp3Nb[byteIndex] & 0x0f);
            if (nibble > 9)
                throw new MVSDatatypeException(INVALID_DIGIT_NIBBLE, (byteIndex + 1) * 2);
            outStrng.append(digits[nibble]);
        }

        // 2. Dernier octet (qui contient le signe)
        // a. Premier demi-octet (= chiffre)
      nibble = (pComp3Nb[byteIndex] & 0xf0) >>> 4;
        if (nibble > 9)
            throw new MVSDatatypeException(INVALID_DIGIT_NIBBLE, byteIndex * 2 + 1);
        outStrng.append(digits[nibble]);
        // b. Deuxieme demi-octet (= signe)
        nibble = (pComp3Nb[byteIndex] & 0x0f);
        if (nibble < 10)
            throw new MVSDatatypeException(INVALID_SIGN_NIBBLE);
        if (11 == nibble || 13 == nibble)
            outStrng.insert(0, '-');
        else
            outStrng.insert(0, '0'); // pas de signe '+' pour permettre le
        // parsing
        // en nombre entier
        return (outStrng.toString());
    }

    /**
     * Converts String to packed decimal number. Decimal points, commas and
     * spaces are ignored. Sign character is processed. Avoid multiple signs.
     * Characters other than digits are invalid and will cause Comp3Exception.
     * Comma, blank, period, dollar sign and plus are ignored. Scaling and
     * exponents are not valid.
     * 
     * @param pString number representation String to convert
     * @param pBytesNb required size for the returned array.
     * @return byte array of packed decimal, with the required length.
     * @throws MVSDatatypeException Too many digits in input string.
     */
    public static byte[] stringToComp3(String pString, int pBytesNb) throws MVSDatatypeException {

        // Verifions que la chaine en entree est un entier comprenant au moins
        // un chiffre
        Pattern pattern = Pattern.compile("[-+]?[0-9]+");
        Matcher matcher = pattern.matcher(pString);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(pString + " n'est pas un nombre entier.");
        }

        // Verifions que l'entier en entree n'est pas trop grand par rapport au
        // nombre d'octets du champ compacte en sortie
        int strgLen = pString.length();
        char c0 = pString.charAt(0);
        if (c0 == '+' || c0 == '-') {
            if ((strgLen / 2 + strgLen % 2) > pBytesNb) {
                throw new MVSDatatypeException(NUMBER_TOO_BIG, pString, pBytesNb);
            }
        } else if (((strgLen + 1) / 2 + (strgLen + 1) % 2) > pBytesNb) {
            throw new MVSDatatypeException(NUMBER_TOO_BIG, pString, pBytesNb);
        }

        char cha;
        int stringIndex = strgLen - 1;
        // Tableau d'octets en sortie
        byte[] comp3 = new byte[pBytesNb];
        byte nibble;
        int comp3LastIndex = pBytesNb - 1;
        int comp3Index = comp3LastIndex;
        boolean rightNibble = false;
        comp3[comp3LastIndex] = 12; // par defaut, positif

        while (stringIndex > -1) {
            cha = pString.charAt(stringIndex);
            if ('0' <= cha && '9' >= cha) {
                nibble = (byte ) (cha - '0');
                if (rightNibble) { // demi-octet droit
                    comp3[comp3Index] = nibble;
                    rightNibble = false;
                } else { // demi-octet gauche
                    // Demi-octet gauche : les bits sont decales quatre fois
                    // vers la gauche, et les
                    // bits manquant a droite sont remplaces par des zeros.
                    // Puis operation OR avec l'octet du tableau, qui contient
                    // deja le demi-octet droit.
                    comp3[comp3Index] = (byte ) (comp3[comp3Index] | nibble << 4);
                    rightNibble = true;
                    --comp3Index; // octet suivant
                }
                --stringIndex; // caractere suivant
            } else {
                switch (cha) {
                    case '+':
                        // positif par defaut --> rien a faire
                        --stringIndex; // caractere suivant
                        break;
                    case '-':
                        comp3[comp3LastIndex] = (byte ) (comp3[comp3LastIndex] | 0x01);
                        --stringIndex; // caractere suivant
                        break;
                }
            }
        }
        return (comp3);
    }
}
