/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.IValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.StringValidator;

/**
 * Représente un champ d'un enregistrement.
 * 
 * @author amd
 */
public final class ConstantFixedField extends AbstractFixedField {

    // pour la création du prototype d'enregistrement (parser)
    /**
     * @param pLabel
     * @param pDescription 
     * @param pStart Position du premier caractère de ce champ dans
     *            l'enregistrement.
     * @param pLength Nombre de caractères de ce champ.
     * @param pConstant
     *
     */
    public ConstantFixedField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, 
                    StringBuffer pConstant) {
        //NB : la validation schematron garantit que la valeur a une longueur égale à length
        label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        value = pConstant;
        validator = new StringValidator(pConstant.toString());
    }
    
    private ConstantFixedField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, 
            IValidator pValidator, StringBuffer pConstant) {
		label = pLabel;
		description = pDescription;
		start = pStart;
		length = pLength;
		value = pConstant;
		validator = pValidator;
	}

    @Override
    public Object clone() {
    	// We keep the same validator which contains the constant value
        return new ConstantFixedField(label, description, start, length, validator, value );
    }

	@Override
	public FixedFieldType getFieldType() {
		return FixedFieldType.constant;
	}

}
