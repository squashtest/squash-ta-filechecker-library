/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.descriptor.checker;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.squashtest.ta.filechecker.library.facade.FileType;
import org.squashtest.ta.filechecker.library.utils.ConfigurationException;
import org.squashtest.ta.filechecker.library.utils.Constants;

/**
 * @author amd
 */
public class DescriptorsSchemas {

    private static DescriptorsSchemas uniqueInstance;

    private Map<String, DescriptorSchema> descriptorsSchemas;

    /**
     * @return l'unique instance de DescriptorsSchemas (singleton)
     * @throws IOException si une exception survient :
     * <ul>
     *     <li>lors de l'accès au fichier de mapping schéma de
     *             descripteur / type de fichier décrit par le descripteur</li>
     *     <li>lors de l'accès à un schéma</li>
     * </ul>
     */
    public static DescriptorsSchemas getInstance() throws IOException {
        if (uniqueInstance == null) {
            uniqueInstance = new DescriptorsSchemas();
        }
        return uniqueInstance;
    }

    /**
     * private default constructor.
     * 
     * @throws IOException si une exception survient :
     * <ul>
     *     <li>lors de l'accès au fichier de mapping schéma de
     *             descripteur / type de fichier décrit par le descripteur</li>
     *     <li>lors de l'accès à un schéma</li>
     * </ul>
     */
    private DescriptorsSchemas() throws IOException {
        descriptorsSchemas = new HashMap<String, DescriptorSchema>();
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.SCHEMA_FILE_2_NAME_MAPPING);
        if (stream == null) {
            throw new ConfigurationException("Le fichier \"{}\" est introuvable.", Constants.SCHEMA_FILE_2_NAME_MAPPING);
        }
        Properties props = new Properties();
        props.load(stream);
        Set<Object> fileNames = props.keySet();
        for (Object name : fileNames) {
            String fileName = (String ) name;
            URL url = Thread.currentThread().getContextClassLoader().getResource(Constants.SCHEMA_DIR + fileName);
            //String uri = getSchemaUri(url);
            FileType type = FileType.getType((String ) props.get(fileName));
            DescriptorSchema dsc = new DescriptorSchema(type, url);
            descriptorsSchemas.put(fileName, dsc);
        }
    }

    public FileType getFileType(String schemaName) {
        FileType type = null;
        DescriptorSchema dsc = descriptorsSchemas.get(schemaName);
        if (dsc == null) {
            throwInvalidUriException(schemaName);
        }
        type = dsc.getFileType();
        return type;
    }

    private void throwInvalidUriException(String schemaName) {

        StringBuilder buf = new StringBuilder();

        buf.append("Le schéma \"");
        buf.append(schemaName);
        buf.append("\" n'existe pas. Les schémas autorisés sont : \"");

        Set<String> schemaUris = descriptorsSchemas.keySet();
        for (String schemaUri : schemaUris) {
            buf.append(Constants.LINE_SEPARATOR);
            buf.append(schemaUri);
        }
        throw new IllegalArgumentException(buf.toString());

    }

    public URL getSchemaUrl(String schemaName) {
        URL url = null;
        DescriptorSchema dsc = descriptorsSchemas.get(schemaName);
        if (dsc != null) {
            url = dsc.getUrl();
        }
        return url;
    }

    // -----------------------------
    // CLASSE INTERNE
    // -----------------------------
    private class DescriptorSchema {

        private URL url;

        private FileType fileType;

        private DescriptorSchema(FileType pFileType, URL pUrl) {
            fileType = pFileType;
            url = pUrl;
        }

        private URL getUrl() {
            return url;
        }

        private FileType getFileType() {
            return fileType;
        }

    }

}
