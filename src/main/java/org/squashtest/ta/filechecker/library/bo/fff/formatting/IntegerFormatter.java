/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import java.text.DecimalFormat;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

/**
 * @author amd
 *
 */
public class IntegerFormatter implements IFormatter<Integer> {

    private int length;
    private boolean nullable;
    private DecimalFormat decimalFormat;
    
    public IntegerFormatter(DecimalFormat pFormat, int pLength, boolean pNullable) {
        decimalFormat = pFormat;
        length = pLength;
        nullable = pNullable;
    }

    /* (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.bo.fff.formatting.IFormatter#format(java.lang.Object)
     */
    public StringBuffer format(Integer pValue) throws InvalidSyntaxException {
        StringBuffer newValue = new StringBuffer(length);
        if (pValue==null){
            if (nullable){
                for (int i = 0; i<length; i++){
                    newValue.append(' ');
                }
            }else{
                throw new InvalidSyntaxException("Champ obligatoire.");
            }
        }else{
            String decimal = decimalFormat.format(pValue);
            //TODO : doit-on faire cette vérif ici ou seulement plus tard, à la validation?
            if (decimal.length()!=length){
                throw new InvalidSyntaxException("Le champ formatté n'a pas le nombre de caractères requis.");
            }
            newValue = new StringBuffer(decimal);
        }
        return newValue;
    }

}
