/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.utils.mvs;

import org.slf4j.helpers.MessageFormatter;

/**
 * @author amd
 *
 */
@SuppressWarnings("serial")
public class MVSDatatypeException extends RuntimeException {

    /**
     * Constructor with cause.
     * 
     * @param pCause the root cause exception this one is encapsulating.
     */
    public MVSDatatypeException(Throwable pCause) {
        super(pCause);
    }

    /**
     * Constructor with message.
     * 
     * @param pMsg Message d'erreur paramétrable (chaque paramètre est remplacé
     *            par {}).
     * @param pParams Paramètres du message d'erreur, s'il y en a.
     */
    public MVSDatatypeException(String pMsg, Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage());
    }

    /**
     * Constructor with cause and message.
     * 
     * @param pCause the root cause exception this one is encapsulating.
     * @param pMsg parametrized error message (the parameter place holders are empty brace pairs ({}).
     * @param pParams Error message parameters.
     */
    public MVSDatatypeException(Throwable pCause, String pMsg, Object... pParams) {
        super(MessageFormatter.arrayFormat(pMsg, pParams).getMessage(), pCause);
    }

}