/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;

import java.text.DecimalFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.DecimalFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.FormatterInitializationException;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.LocalDateFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.Padder;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AbstractFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AutonumberField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.Comp3Field;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.ConstantFixedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.FixedFieldType;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.IdField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.InputField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.SignedZoneDecimalField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.UnsignedZoneDecimalField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.UnusedField;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.DateValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.DoubleValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.IValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.RegexpValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.ValidatorInitializationException;

/**
 * @author amd
 */

public class FieldFactory {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FieldFactory.class);

    private TreeModelFactory treeModelFactory;

    private String type;

    private int start;

    private int fieldLength;

    private String format;

    private boolean nullable = false;

    private String sequence;

    private StringBuffer label;

    private StringBuffer description;

    private int decNb;

    public void setTreeModelFactory(TreeModelFactory pTreeModelFactory) {
        treeModelFactory = pTreeModelFactory;
    }

    public void setType(String pType) throws InvalidDescriptorException {
        type = pType;
    }

    public void setStart(int pStart) {
        start = pStart;
    }

    public void setLength(int pLength) {
        fieldLength = pLength;
    }

    public void setFormat(String pFormat) {
        format = pFormat;
    }

    public void setNullable(boolean pNullable) {
        nullable = pNullable;
    }

    public void setSequence(String pSequence) {
        sequence = pSequence;
    }

    public void setLabel(String pLabel) {
        label = new StringBuffer(pLabel);
    }

    public void setDescription(String pDescription) {
        description = new StringBuffer(pDescription);
    }

    public void setDecimalNb(int pDecimalNb) {
        decNb = pDecimalNb;
    }

    // ------------------------------------------------------------------

    public AbstractFixedField createField() throws InvalidDescriptorException  {

        AbstractFixedField field = null;
        IValidator validator;

        FixedFieldType fieldType = findType(type);

        try {
            switch (fieldType) {

                case id:
                    field = new IdField(label, description, start, fieldLength, new StringBuffer(format));
                    break;

                case constant:
                    field = new ConstantFixedField(label, description, start, fieldLength, new StringBuffer(format));
                    break;

                case unused:
                    field = new UnusedField(label, description, start, fieldLength);
                    break;

                case text:
                    Padder formatter1 = new Padder(' ', false, fieldLength, nullable);
                    validator = new RegexpValidator(format, nullable);
                field = new InputField<String>(label, description, start, fieldLength, formatter1, validator,FixedFieldType.text);
                    break;

                case date:
                    LocalDateFormatter dateFrmtr = new LocalDateFormatter(format, fieldLength, nullable);
                    validator = new DateValidator(format, nullable);
                field = new InputField<Date>(label, description, start, fieldLength, dateFrmtr, validator,FixedFieldType.date);
                    break;

                case decimal:
                    DecimalFormat decFormat = new DecimalFormat(format);
                    DecimalFormatter formatter3 = new DecimalFormatter(decFormat, fieldLength, nullable);
                    validator = new DoubleValidator(format, nullable);
                    field = new InputField<Double>(label, description, start, fieldLength, formatter3, validator, FixedFieldType.decimal);
                    break;

                case autonumber:
                    field = new AutonumberField(label, description, start, fieldLength, treeModelFactory.getSequence(sequence));
                    break;

                case comp3:
                    field = new Comp3Field(label, description, start, fieldLength, decNb, nullable, treeModelFactory.getEncoding());
                    break;

                case signedZoneDecimal:
                    field = new SignedZoneDecimalField(label, description, start, fieldLength, decNb, treeModelFactory.getEncoding());
                    break;

                case unsignedZoneDecimal:
                    field = new UnsignedZoneDecimalField(label, description, start, fieldLength, decNb);
                    break;

                default:
                    break;

            }
        } catch (ValidatorInitializationException e) {
            throw new InvalidDescriptorException(e, e.getMessage());
        } catch (FormatterInitializationException e) {
            throw new InvalidDescriptorException(e, e.getMessage());
        }
        return field;
    }

    private FixedFieldType findType(String pType) throws InvalidDescriptorException {
        // la validation xsd garantit que l'attribut type est renseigné,
        // et a une des valeurs suivantes : id, constant, unused, text, date,
        // decimal, autonumber, comp3;
        FixedFieldType type = null;
        FixedFieldType[] types = FixedFieldType.values();
        int nb = types.length;
        int i = 0;
        boolean notFound = true;
        while (notFound && i < nb) {
            if (pType.equals(types[i].toString())) {
                type = types[i];
                notFound = false;
            }
            i++;
        }
        // If not founds, that means the type is defined in the XSD but not in the api
        // Currently the "integer" type is still defined in the schema whereas it has already been removed in the code.
        if (notFound) {
            throw new InvalidDescriptorException("{} is not a valid field type", pType);
        }
        return type;
    }

}
