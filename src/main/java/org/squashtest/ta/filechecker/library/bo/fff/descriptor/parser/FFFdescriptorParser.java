/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;

import java.io.IOException;
import java.net.URL;

import org.apache.commons.digester.Digester;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure.AndExpression;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure.LitteralExpression;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure.OrExpression;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;

/**
 * Présente une méthode qui crée un prototype de fichier séquentiel à partir
 * d'un fichier xml de description.
 */
public class FFFdescriptorParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(FFFdescriptorParser.class);

    private FFFdescriptorParser() {
    }

    /**
     * <p>Crée un <code>Prototype</code> (prototype de fichier séquentiel).</p>
     * La structure générale du prototype est définie grâce au fichier xml dont
     * le nom est passé en argument du contructeur. Dans le cas où un fichier de
     * properties du même nom est également présent, il permet d'associer à
     * chaque enregistrement composite du prototype le motif (attribut
     * "pattern") qui lui correspond. Ce motif représente les combinaisons
     * autorisées pour les enfants de l'enregistrement composite.
     * 
     * @param pUrl nom du fichier décrivant la structure du fichier
     *            séquentiel
     * @throws IOException - si une erreur survient lors de l'accès au fichier
     *             xml ou au fichier de properties. 
     *          Sera une {@link java.io.FileNotFoundException} si le fichier xml est introuvable.
     * @throws SAXException - si une erreur survient lors du parsing par le
     *             digester du contenu du fichier xml.
     * @throws InvalidDescriptorException 
     */
    public static FFFrecordsTemplate getRecordsTemplate(URL pUrl) throws IOException, SAXException, InvalidDescriptorException {
        if (pUrl == null) {
            throw new IllegalArgumentException("Url null");
        }
        TreeModelFactory factory = process(pUrl);
        return factory.createTreeModel();
    }

    private static TreeModelFactory process(final URL pUrl) throws IOException, SAXException {
        // 2. Traitement du fichier de description
        TreeModelFactory treeModelFactory = new TreeModelFactory();
        LOGGER.info("Traitement du fichier de description {} ", pUrl);
        Digester d = new Digester();
        d.setValidating(false);
        d.setNamespaceAware(true);
        d.push(treeModelFactory);
        addRules(d);
        d.parse(pUrl);
        return treeModelFactory;
    }

    /**
     * Affected au digester les règles qui permettront d'effectuer le mapping
     * xml / objet.
     */
    private static void addRules(final Digester pDigester) {
        addRootRules(pDigester);
        addSequenceRules(pDigester);
        addLeafRules(pDigester);
        addFieldRules(pDigester);
        addCompositeRules(pDigester);
    }

    /**
     * @param pDigester
     */
    private static void addRootRules(Digester pDigester) {
        pDigester.addSetProperties("root");
        pDigester.addCallMethod("root", "setEncoding", 1);
        pDigester.addCallParam("root", 0, "encoding");
    }

    private static void addSequenceRules(final Digester pDigester) {
        // On passer par une factory pour pouvoir utiliser les
        // attributs de l'élément [sequence] comme arguments du constructeur
        // de la classe "Sequence"
        pDigester.addFactoryCreate("*/sequences/sequence", SequenceFactory.class);
        pDigester.addSetRoot("*/sequences/sequence", "addSequence");
    }

    private static void addLeafRules(Digester pDigester) {
        // Création d'un objet LeafModelFactory
        pDigester.addObjectCreate("*/leafRecord", LeafModelFactory.class);
        // Ajout de l'attribut 'name'
        pDigester.addSetProperties("*/leafRecord");
        // Ajout des éléments enfants "label" et "description"
        String[] xmlNames = {"label", "description", "fields", "reset" };
        String[] javaNames = {"label", "description" };
        pDigester.addSetNestedProperties("*/leafRecord", xmlNames, javaNames);
        // Ajout des séquences à réinitialiser
        pDigester.addCallMethod("*/reset/sequenceId", "addSequenceToReset", 1);
        pDigester.addCallParam("*/reset/sequenceId", 0);
        // Ajout de l'objet situé en bas de la pile = TreeModelFactory (besoin
        // pour
        // la référence aux séquences à réinitialiser)
        Class< ? >[] clazz = {TreeModelFactory.class };
        pDigester.addCallMethod("*/leafRecord", "setTreeModelFactory", 1, clazz);
        pDigester.addCallParam("*/leafRecord", 0, 1);
        // Ajout de l'objet LeafModelFactory à l'objet en bas de la pile =
        // TreeModelFactory
        pDigester.addSetRoot("*/leafRecord", "addLeafModelFactory");
    }

    private static void addFieldRules(Digester pDigester) {
        pDigester.addObjectCreate("*/field", FieldFactory.class);
        // Ajout des attributs de l'élément "field"
        pDigester.addSetProperties("*/field");
        // Ajout des éléments enfants "libelle" et "desc"
        pDigester.addSetNestedProperties("*/field");
        // Ajout à l'objet LeafModelFactory situé en dessous
        pDigester.addSetNext("*/field", "addFieldFactory");
        // Ajout de l'objet situé en bas de la pile = TreeModelFactory (besoin
        // pour
        // la référence aux séquences à réinitialiser)
        Class< ? >[] clazz = {TreeModelFactory.class };
        pDigester.addCallMethod("*/field", "setTreeModelFactory", 1, clazz);
        pDigester.addCallParam("*/field", 0, 2);

    }

    private static void addCompositeRules(Digester pDigester) {
        pDigester.addObjectCreate("*/compositeRecord", CompositeModelFactory.class);
        // Ajout des attributs de l'élément "compositeRecord"
        pDigester.addSetProperties("*/compositeRecord");
        // Ajout des éléments enfants 'openingRecord' et 'closingRecord'
        pDigester.addBeanPropertySetter("*/compositeRecord/openingRecord", "openingName");
        pDigester.addBeanPropertySetter("*/compositeRecord/closingRecord", "closingName");
         // arbre syntaxique
         pDigester.addObjectCreate("*/record", LitteralExpression.class);
         pDigester.addBeanPropertySetter("*/record", "recordName");
         pDigester.addSetNext("*/record", "addExpression");
         pDigester.addObjectCreate("*/and", AndExpression.class);
         pDigester.addSetNext("*/and", "addExpression");
         pDigester.addObjectCreate("*/or", OrExpression.class);
         pDigester.addSetNext("*/or", "addExpression");
         pDigester.addFactoryCreate("*/repeat", RepeatFactory.class);
         // Ajout de l'arbre syntaxique à l'objet CompositeModelFactory
         pDigester.addSetNext("*/repeat", "addExpression");
         // Ajout de l'objet CompositeModelFactory à l'objet en bas de la pile = TreeModelFactory
         pDigester.addSetRoot("*/compositeRecord", "addCompositeModelFactory");
    }

}
