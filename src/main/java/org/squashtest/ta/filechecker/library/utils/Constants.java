/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.utils;

/**
 * @author amd
 *
 */
public class Constants {

    //XXX TODO : externaliser dans un fichier de conf?
    
    //--------------------
    // DIRECTORIES & FILES
    //--------------------
    
    public static final String SCHEMA_DIR = "schemas/";
    
    public static final String SCHEMA_FILE_2_NAME_MAPPING = "schemas/Mapping.properties";

    public static final String XSD_2_SCHMTRN_STYLESHEET = "iso/schematron/xslt2/XSD2Schtrn.xsl";

    public static final String SVRL_STYLESHEET = "iso/schematron/xslt2/iso_svrl_for_xslt2.xsl";
    
    public static final String DESCRIPTOR_DIR = "filechecker/descriptors/";
    
    public static final String XPATH_QUERIES_DIR = "filechecker/xpath/queries/";
    
    public static final String DOWNLOAD_DIR = "data/ftp/download/";
    
    //--------------------
    // 
    //--------------------
    
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    public static final char DECIMAL_SEPARATOR = '.';

}
