/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.iface;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

public abstract class AbstractField implements Cloneable {

    /** Field label. Could be <code>null</code>. */
    protected StringBuffer label;

    /** Field description. Could be <code>null</code>. */
    protected StringBuffer description;

    /** Field value */
    protected StringBuffer value;

    /** @return The field label */
    public StringBuffer getLabel() {
        return label;
    }

    /**
     * @param label The new field label
     */
    public void setLabel(StringBuffer label) {
        this.label = label;
    }

    /** @return The field description*/
    public StringBuffer getDescription() {
        return description;
    }

    /**
     * @param description The new field description
     */
    public void setDescription(StringBuffer description) {
        this.description = description;
    }

    /*public String toFileFormat() {
        return value.toString();
    }*/

    /** @return The field value */
    public StringBuffer getValue() {
        return value;
    }

    public abstract Object clone();

    /**
     * @throws InvalidSyntaxException
     */
    // hook (might be replace by BF ^^)
    public void validate() throws InvalidSyntaxException {
    }

    @Override
    public String toString() {
        return (value != null) ? value.toString() : null;
    }

}
