/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;
import org.squashtest.ta.filechecker.library.bo.fff.template.CompositeTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.fff.template.LeafTemplate;

public class TreeModelFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(TreeModelFactory.class);

    private String name;
    
    private StringBuffer encoding;
    
    private int bytesPerLine;

    private boolean flat, multiRecords, isBinary;
    
    private Map<String, Sequence> sequences;

    private Map<String, LeafModelFactory> leafModelFactories;

    private Map<String, CompositeModelFactory> compositeModelFactories;

    // ------------------------------------------------------------------------------

    TreeModelFactory() {
        sequences = new HashMap<String, Sequence>() ;
        leafModelFactories= new HashMap<String, LeafModelFactory>();
        compositeModelFactories = new HashMap<String, CompositeModelFactory>();
    }

    // ------------------------------------------------------------------------------
    //PARSER
    
    public void setFlat(boolean pFlat) {
        flat = pFlat;
    }

    public void setMultiRecords(boolean pMultiRecords) {
        multiRecords = pMultiRecords;
    }

    public void setName(String pName) {
        name = pName;
    }
    
    public void setBinary(boolean pBinary) {
        isBinary = pBinary;
    }

    public void setEncoding(String pEncoding) {
        encoding = new StringBuffer(pEncoding);
    }

    public void setBytesPerLine(int pBytesPerLine) {
        bytesPerLine = pBytesPerLine;
    }

    /** @param pSequence séquence d'auto-incrémentation à ajouter */
    public void addSequence(Sequence pSequence) {
        String sequenceName = pSequence.getId().toString();
        sequences.put(sequenceName, pSequence);
    }
    
    /** @param pLeafModelFactory enregistrement feuille à ajouter */
    public void addLeafModelFactory(LeafModelFactory pLeafModelFactory) {
        String lName = pLeafModelFactory.getName();
        leafModelFactories.put(lName, pLeafModelFactory);
        LOGGER.debug("Ajout de la feuille \"{}\"", lName);
    }

    /** @param pCompositeModelFactory enregistrement composite à ajouter */
    public void addCompositeModelFactory(CompositeModelFactory pCompositeModelFactory) {
        String cName = pCompositeModelFactory.getName();
        compositeModelFactories.put(cName, pCompositeModelFactory);
        LOGGER.debug("Ajout du composite \"{}\"", cName);
    }
    
    // ------------------------------------------------------------------------------
    
    // utilisé par LeafModelFactory 
    Sequence getSequence(String sequenceId) {
        return sequences.get(sequenceId);
    }
    
    public Map<String, Sequence> getSequences() {
        return sequences;
    }

    public void setSequences(Map<String, Sequence> pSequences) {
        sequences = pSequences;
    }

    public Map<String, LeafModelFactory> getLeafModelFactories() {
        return leafModelFactories;
    }

    public void setLeafModelFactories(Map<String, LeafModelFactory> pLeafModelFactories) {
        leafModelFactories = pLeafModelFactories;
    }

    public Map<String, CompositeModelFactory> getCompositeModelFactories() {
        return compositeModelFactories;
    }

    public void setCompositeModelFactories(Map<String, CompositeModelFactory> pCompositeModelFactories) {
        compositeModelFactories = pCompositeModelFactories;
    }

    public String getName() {
        return name;
    }

    public int getBytesPerLine() {
        return bytesPerLine;
    }

    public boolean isFlat() {
        return flat;
    }

    public boolean isMultiRecords() {
        return multiRecords;
    }

    public boolean isBinary() {
        return isBinary;
    }
    
    public StringBuffer getEncoding() {
        return encoding;
    }

    
    // ------------------------------------------------------------------------------

    public FFFrecordsTemplate createTreeModel() throws InvalidDescriptorException{
        List<LeafTemplate> leafModels = createLeafModels();
        Map<String, CompositeTemplate> compositeModels = createCompositeModels();
        FFFrecordsTemplate treeModel = new FFFrecordsTemplate(name, isBinary, bytesPerLine, encoding, flat, multiRecords, leafModels, compositeModels);
        return treeModel;
    }

    // ------------------------------------------------------------------------------
    
    private List<LeafTemplate> createLeafModels() throws InvalidDescriptorException {
        List<LeafTemplate> leafModels = new ArrayList<LeafTemplate>();
        Collection<LeafModelFactory> factories = leafModelFactories.values();
        for (LeafModelFactory factory : factories){
            LeafTemplate leafModel = factory.createLeafModel();
            leafModels.add(leafModel);
        }
        return leafModels;
    }
    
    private Map<String, CompositeTemplate> createCompositeModels() {

        Map<String, CompositeTemplate> compositeModels = new HashMap<String, CompositeTemplate>();
        
        Collection<CompositeModelFactory> factories = compositeModelFactories.values();
        for (CompositeModelFactory factory : factories){
            if (factory.getClosingName()==null){
                List<String> descendants = findDescendants(factory);
                factory.setDescendants(descendants);                
            }
            CompositeTemplate compositeModel = factory.createCompositeModel();
            compositeModels.put(compositeModel.getOpeningName(), compositeModel);
        }
        
        return compositeModels;
    }

    private List<String> findDescendants(CompositeModelFactory pFactory) {

        List<String> descendants = new ArrayList<String>();
        
//        // 1. enregistrement ouvrant du composite
//        descendants.add(pFactory.getOpeningName());
        
        // 2. enregistrements enfants du composite...
        List<String> operands = pFactory.getChildrenPattern().getOperands();
        
        for (String operand : operands) {
            CompositeModelFactory compositeModelFactory = compositeModelFactories.get(operand);
            // 2.1. ... qui sont des enregistrements feuilles
            if (compositeModelFactory==null){
                descendants.add(operand);
            // 2.2. ... qui sont des eux-même des enregistrements composites
            }else{
                // --> on prend leur enregistrement ouvrant
                descendants.add(compositeModelFactory.getOpeningName());
            }
        }
        
        return descendants;
    }


    
    //-----------------
    

    
}
