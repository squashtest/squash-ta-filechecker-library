/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.IFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

/**
 * @author amd
 */
public abstract class AbstractVariableFixedField<E> extends AbstractFixedField {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractVariableFixedField.class);
    protected IFormatter<E> formatter;

    /**
     * Affecte au champ une valeur, obtenue par formattage de l'argument en
     * entrée.
     * 
     * @param pValue valeur du champ
     */
    public void setValue(E pValue) throws InvalidSyntaxException{
        value = formatter.format(pValue);
    }

}
