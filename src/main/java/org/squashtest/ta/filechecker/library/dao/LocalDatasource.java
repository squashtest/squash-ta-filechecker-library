/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.dao;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.squashtest.ta.filechecker.library.utils.Constants;

/**
 * @author amd
 */
public class LocalDatasource implements IDatasource {

    /** */
    private String filePath;

    private boolean isBinary;

    private int bytesPerRecord;

    private StringBuffer encoding;
    
    private int maxLineToRead = Integer.MAX_VALUE;

    /**
     * Crée une nouvelle instance de {@link LocalDatasource}
     * 
     * @param pFileUri URI du fichier séquentiel à lire et/ou écrire.
     */
    public LocalDatasource(URI pFileUri) {
        filePath = pFileUri.getPath();
    }

    /**
     * Crée une nouvelle instance de {@link LocalDatasource}
     * 
     * @param pFileUri URI du fichier séquentiel à lire et/ou écrire.
     * @param pIsBinary Flag qui indique s'il s'agit d'un fichier binaire ou
     *            texte
     * @param pBytesPerRecord Nb d'octets par enregistrement, dans le cas d'un
     *            fichier binaire
     * @param pEncoding Encodage, dans le cas d'un fichier binaire
     */
    public LocalDatasource(URI pFileUri, boolean pIsBinary, int pBytesPerRecord, StringBuffer pEncoding) {
        bytesPerRecord = pBytesPerRecord;
        encoding = pEncoding;
        filePath = pFileUri.getPath();
        isBinary = pIsBinary;
    }
    
    /**
     * Crée une nouvelle instance de {@link LocalDatasource}
     * 
     * @param pFileUri URI du fichier séquentiel à lire et/ou écrire.
     * @param pIsBinary Flag qui indique s'il s'agit d'un fichier binaire ou
     *            texte
     * @param pBytesPerRecord Nb d'octets par enregistrement, dans le cas d'un
     *            fichier binaire
     * @param pEncoding Encodage, dans le cas d'un fichier binaire
     */
    public LocalDatasource(URI pFileUri, boolean pIsBinary, int pBytesPerRecord, StringBuffer pEncoding, int pMaxLineToRead) {
        bytesPerRecord = pBytesPerRecord;
        encoding = pEncoding;
        filePath = pFileUri.getPath();
        isBinary = pIsBinary;
        maxLineToRead = pMaxLineToRead;
    }

    /**
     * Lit un fichier séquentiel
     * 
     * @return une collection de chaînes de caractères correspondant aux lignes
     *         du fichier séquentiel lu.
     * @throws IOException si une IOException survient lors de la lecture d'une
     *             ligne. L'exception sera une {@link java.io.FileNotFoundException} si le fichier n'existe pas, est un
     *             répertoire, ou ne peut être lu pour quelqu'autre raison que
     *             ce soit.
     */
    public List<String> read() throws IOException {
        if (isBinary)
            return readBinaryFile();
        return readTextFile();
    }

    /*
     * (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.dao.IDatasource#write(java.util.List)
     */
    public void write(List<String> pLines) throws IOException {
        if (isBinary) {
            writeBinaryFile(pLines);
        } else {
            writeTextFile(pLines);
        }
    }

    /**
     * @param pLines
     */
    private void writeBinaryFile(List<String> pLines) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException();
    }

    /**
     * Ecrit une collection de lignes dans un fichier. Si le fichier n'existe
     * pas préalablement, il sera créé
     * 
     * @param lines = lignes à écrire dans le fichier
     * @throws IOException if the named file exists but is a directory rather
     *             than a regular file, does not exist but cannot be created, or
     *             cannot be opened for any other reason<br>
     *             - if an I/O error occurs
     */
    public void writeTextFile(List<String> lines) throws IOException {
        FileWriter fw = new FileWriter(filePath);
        int size = lines.size();
        for (int i = 0; i < size; i++) {
            String line = lines.get(i);
            if (!line.endsWith(Constants.LINE_SEPARATOR)) {
                line += Constants.LINE_SEPARATOR;
            }
            fw.write(line);
        }
        fw.flush();
    }

    /**
     * Reads the backing binary file into a {@link List} of Strings.
     * @return {@link List} de lignes
     * @throws IOException si une erreur survient lors de l'accès au fichier
     */
    private List<String> readTextFile() throws IOException {
        List<String> lines = new ArrayList<String>();
        BufferedReader lineReader;
        if(this.encoding==null || this.encoding.length()==0){
        	lineReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
        }else{
        	lineReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath),this.encoding.toString()));
        }
        String line;
        int index = 0;
        while (null != (line = lineReader.readLine()) && index < maxLineToRead) {
            lines.add(line);
            index++;
        }
        lineReader.close();
        return lines;
    }

    /**
     * Reads the backing binary file into a {@link List} of Strings.
     * @return {@link List} de lignes
     * @throws IOException si une erreur survient lors de l'accès au fichier
     */
    public List<String> readBinaryFile() throws IOException {
        File file = new File(filePath);
        List<String> lines = new ArrayList<String>();
        RandomAccessFile raf = new RandomAccessFile(file, "r");
        int lineNb;
        int totLineNb = (int ) (raf.length() / bytesPerRecord);
        if (totLineNb < maxLineToRead){
            lineNb = totLineNb;
        }else{
            lineNb = maxLineToRead;
        }
        byte[] line = new byte[bytesPerRecord];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int index = 0;
        while (index < lineNb ) {
            raf.readFully(line);
            baos.write(line);
            lines.add(baos.toString(encoding.toString()));
            baos.reset();
            index++;
        }
        raf.close();
        return lines;
    }

}
