/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.descriptor.checker;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.schematron.SchematronValidator;
import org.squashtest.ta.filechecker.library.utils.Constants;
import org.squashtest.ta.filechecker.library.utils.SchemaValidationErrorHandler;
import org.xml.sax.SAXException;

/**
 * @author amd
 *
 */
public class DescriptorChecker {
    
    private static final SchematronValidator schematronValidator = SchematronValidator.getInstance();

    private static final Logger LOGGER = LoggerFactory.getLogger(DescriptorChecker.class);
    
    private URL xmlFileUrl;
    
    private URL schemaUrl;

    public DescriptorChecker(URL pXmlFileUrl, URL pSchemaUrl) {
        xmlFileUrl = pXmlFileUrl;
        schemaUrl = pSchemaUrl;
    }

    /**
     * Valide un fichier xml de description de fichiers à champs fixes
     * 
     * @throws IOException : si une erreur survient lors de l'accès au fichier de validation ou au fichier à valider
     * @throws SAXException : si le schéma de validation est incorrect
     * @throws InvalidDescriptorException 
     */
    public void validate() throws  IOException, SAXException, InvalidDescriptorException {

        // a. Validation xsd (validation structurelle et syntaxique)
        LOGGER.info("Validation structurelle et syntaxique du fichier de description \"{}\"", xmlFileUrl);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(schemaUrl);
        Validator validator = schema.newValidator();
        SchemaValidationErrorHandler errHandler = new SchemaValidationErrorHandler();
        validator.setErrorHandler(errHandler);
        Source source = new StreamSource(new File(xmlFileUrl.getPath()));
        try {
			validator.validate(source);
			List<String> errorList = errHandler.getExceptions();
			errorManager(errorList);
		} catch (SAXException e) {
			throw new InvalidDescriptorException(e, e.getMessage());
		}

        // b. Validation schématron (validation fonctionnelle)
        LOGGER.info("Validation fonctionnelle du fichier de description \"{}\"", xmlFileUrl);
        List<String> errorList = schematronValidator.validate(xmlFileUrl, schemaUrl, true);
        errorManager(errorList);
        
    }
    
    private void errorManager(List<String> errorList)throws InvalidDescriptorException{
    	if (errorList.size() > 0) {
	    	StringBuilder builder = new StringBuilder(xmlFileUrl.toString());
	    	builder.append(" is not a vaild FFF descriptor file");
			builder.append(Constants.LINE_SEPARATOR);
			Iterator<String> errorIt = errorList.iterator();
			while (errorIt.hasNext()) {
				String error = errorIt.next();
				builder.append(error);
				if(errorIt.hasNext())
				{
					builder.append(Constants.LINE_SEPARATOR);
				}
			}
			throw new InvalidDescriptorException(builder.toString());
    	}
    }
    
    
}
