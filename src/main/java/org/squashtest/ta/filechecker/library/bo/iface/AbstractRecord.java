/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.iface;

import java.util.Iterator;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

/**
 * Cf. design pattern "composite"
 * <p>
 * Cette classe joue le rôle d'interface pour tous les enregistrements de la
 * composition, i.e :</p>
 * <ul>
 * <li>les enregistrements composés (<code>CompositeRecord</code>)</li>
 * <li>les enregistrements simples, ou "feuilles" (<code>LeafRecord</code>)</li>
 * </ul>
 * 
 * @author amd
 **/
public abstract class AbstractRecord implements Cloneable {

    // ----------------------------------------------------------------
    // Declarations
    // ----------------------------------------------------------------

    /** Nom de cet enregistrement */
    protected StringBuffer name;

    /** Libellé de cet enregistrement */
    protected StringBuffer label;

    /** Description de cet enregistrement */
    protected StringBuffer description;

    // ----------------------------------------------------------------
    // Constructeur
    // ----------------------------------------------------------------

    /**
     * Crée une nouvelle instance de <code>Record</code>.
     * 
     * @param pName Nom de cet enregistrement
     * @param pLabel Libellé de cet enregistrement
     * @param pDescription Description de cet enregistrement
     */
    protected AbstractRecord(StringBuffer pName, StringBuffer pLabel, StringBuffer pDescription) {
        name = pName;
        label = pLabel;
        description = pDescription;
    }

    // ----------------------------------------------------------------
    // Getters setters
    // ----------------------------------------------------------------

    /** @return Nom de cet enregistrement. */
    public StringBuffer getName() {
        return name;
    }

    /** @return le libellé de l'enregistrement. */
    public StringBuffer getLabel() {
        return label;
    }

    /** @return la description de l'enregistrement. */
    public StringBuffer getDescription() {
        return description;
    }

    // ----------------------------------------------------------------
    // Méthodes propres au design pattern "composite".
    // On implémente un comportement par défaut dans la classe
    // SequentialRecord, qui sera éventuellement surchargé dans les
    // sous-classes.
    // ----------------------------------------------------------------

//    /**
//     * Ajoute un enfant à l'enregistrement.
//     * 
//     * @param pChild enfant à ajouter.
//     * @throws UnsupportedOperationException si l'on appelle cette méthode sur
//     *             un enregistrement "feuille", qui ne peut pas avoir d'enfants.
//     */
//    public void addChild(AbstractRecord pChild) {
//        throw new UnsupportedOperationException();
//    }
//
//    /**
//     * Ajoute un enfant à l'enregistrement, à la position donnée par l'indice
//     * "pIndex"
//     * 
//     * @param pChild enfant à ajouter.
//     * @param pIndex position à laquelle l'enfant doit être ajouté.
//     * @throws UnsupportedOperationException si l'on appelle cette méthode sur
//     *             un enregistrement "feuille", qui ne peut pas avoir d'enfants.
//     */
//    public void addChild(AbstractRecord pChild, int pIndex) {
//        throw new UnsupportedOperationException();
//    }
//
//    /**
//     * Supprime l'enfant "pChild" de l'enregistrement.
//     * 
//     * @param pChild enfant à supprimer.
//     * @throws UnsupportedOperationException si l'on appelle cette méthode sur
//     *             un enregistrement "feuille", qui ne peut pas avoir d'enfants.
//     */
//    public void removeChild(AbstractRecord pChild) {
//        throw new UnsupportedOperationException();
//    }
//
//    /**
//     * Retourne l'enfant situé à la position donnée par l'indice "pIndex"
//     * 
//     * @param pIndex position de l'enfant à retourner
//     * @return l'enfant situé à la position donnée par l'indice "pIndex"
//     * @throws UnsupportedOperationException si l'on appelle cette méthode sur
//     *             un enregistrement "feuille", qui ne peut pas avoir d'enfants.
//     */
//    public AbstractRecord getChildAt(int pIndex) {
//        throw new UnsupportedOperationException();
//    }

	// pour parsing
    /**
     * @return un itérateur sur l'enregistrement (par défaut, retourne un
     *         NullIterator, qui n'itère sur rien).
     */
    public Iterator<AbstractRecord> getIterator() {
        return new NullRecordIterator();
    }

    /**
     * Vérifie que l'enregistrement est valide.
     * @throws InvalidSyntaxException si l'enregistrement n'est pas valide et que le projet est en mode "exception".
     */
    public abstract void validate() throws InvalidSyntaxException;
    
    public boolean validateAutonumbers() throws InvalidSyntaxException{
        return true;
    };

    public abstract void autoincrement() throws InvalidSyntaxException;
    
    @Override
    public abstract String toString();

    @Override
    public abstract Object clone();
    
    
    //----------------------------------------------------------------
    // CLASSE INTERNE
    //----------------------------------------------------------------
    
    /**
     * Itérateur de <code>Record</code> pour les objets n'ayant pas de composants
     * sur lesquels itérer.
     * <p>
     * Cf design pattern composite, qui oblige à traiter de manière identique des
     * éléments simples et des éléments composés
     * 
     * @author amd
     */
    public class NullRecordIterator implements Iterator<AbstractRecord> {
        // XXX javadoc de la classe-mère

        /**
         * Implémentation de la méthode abstraite <code>hasNext</code> de
         * l'interface {@link java.util.Iterator Iterator}.
         * 
         * @return <code>false</code>
         */
        public boolean hasNext() {
            return false;
        }

        /**
         * Implémentation de la méthode abstraite <code>next</code> de l'interface
         * {@link java.util.Iterator Iterator}.
         * 
         * @return <code>null</code>
         **/
        public AbstractRecord next() {
            return null;
        }

        /**
         * Implémentation de la méthode abstraite <code>remove</code> de l'interface
         * {@link java.util.Iterator Iterator}.
         * 
         * @throws UnsupportedOperationException
         **/

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }


}
