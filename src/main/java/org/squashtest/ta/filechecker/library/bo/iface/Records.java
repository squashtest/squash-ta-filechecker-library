/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.iface;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.jxpath.JXPathContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.AutonumberField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.CompositeRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;
import org.squashtest.ta.filechecker.library.bo.fff.records.iterator.RecordsIterator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.facade.InvalidNumberException;
import org.squashtest.ta.filechecker.library.utils.StrngUtil;
import org.squashtest.ta.filechecker.library.utils.xpath.InvalidContentException;
import org.squashtest.ta.filechecker.library.utils.xpath.XpathAssertion;

/**
 * @author amd
 */
@SuppressWarnings("serial")
public class Records extends ArrayList<AbstractRecord> {

	private final static Logger LOGGER = LoggerFactory.getLogger(Records.class);
        
        /**
        * Liste répertoriant les séquences qui doivent être réinitialisées à partir
        * de ce type d'enregistrement
        */
        private List<Sequence> sequencesToReset = new ArrayList<>();

    public void validate() throws InvalidSyntaxException {
        Iterator<AbstractRecord> iterator = iterator();
        while (iterator.hasNext()) {
            iterator.next().validate();
        }
    }

    public void validateSequences() throws InvalidSyntaxException {
        // reset de toutes les séquences avant de commencer?
        for (Sequence seq : sequencesToReset) {
            seq.reset();
        }
        Iterator<AbstractRecord> iterator = iterator();
        int lineNb = 0;
        try {
        	while (iterator.hasNext()) {
        		AbstractRecord record = iterator.next();
                        if (record instanceof LeafRecord) {
        			lineNb = validateAutonumbers((LeafRecord ) record, lineNb);
        		}
	        }
        } catch (InvalidNumberException e) {
			throw new InvalidSyntaxException(e);
		}
    }
    
    public void validateSemantics(Iterator<XpathAssertion> assertionIt) throws InvalidContentException{
    	String query;
    	Object queryResult;
    	while (assertionIt.hasNext()) {
			XpathAssertion xpathAssertion = (XpathAssertion) assertionIt.next();
			query = xpathAssertion.getQuery();
            queryResult = executeXpathQuery(StrngUtil.removeSeparators(query));
            int actual = ((Double) queryResult).intValue();
            int expected = xpathAssertion.getResult();
            if (actual != expected){
            	Object[] args = {xpathAssertion.getQueryName(), actual, expected};
            	throw new InvalidContentException(MessageFormatter.arrayFormat("Query '{}' returned {} record(s) instead of {}.", args).getMessage());
            } 
		}
    }
    
    private Object executeXpathQuery(String pXpath) {
        JXPathContext context = JXPathContext.newContext(this);
        Object result = context.getValue(pXpath);
        return result;
    }

    /**
     * @param pComposite
     * @param pLineNb
     * @return
     * @throws InvalidNumberException 
     */
    @SuppressWarnings("unused")
	private int validateAutonumbers(CompositeRecord pComposite, int pLineNb) throws InvalidNumberException {
        LeafRecord leaf;
        CompositeRecord composite;
        int lineNb = validateAutonumbers(pComposite.getOpeningRecord(), pLineNb);
        for (AbstractRecord record : pComposite.getChildren()) {
            if (record instanceof LeafRecord) {
                leaf = (LeafRecord ) record;
                lineNb = validateAutonumbers(leaf, lineNb);
            } else {
                composite = (CompositeRecord ) record;
                lineNb = validateAutonumbers(composite, lineNb);
            }
        }
        leaf = pComposite.getClosingRecord();
        if (leaf != null) {
            lineNb = validateAutonumbers(leaf, lineNb);
        }
        return lineNb;
    }

    /**
     * @param pLeaf
     * @param pLineNb
     * @throws InvalidNumberException 
     */
    private int validateAutonumbers(LeafRecord pLeaf, int pLineNb) throws InvalidNumberException {

        int lineNb = ++pLineNb;
        LOGGER.debug("Validation des champs autoincrémentés de l'enregistrement {}, ligne {}", pLeaf.getName(), lineNb);

        List<Sequence> sequencesToResetInSection = pLeaf.getSequencesToReset();
        
        List<AutonumberField> fieldsToIncrement = pLeaf.getFieldsToIncrement();
        
        for (Sequence seq : sequencesToResetInSection) {
          seq.reset();
        }
        
        for (AutonumberField auto : fieldsToIncrement) {
            Sequence seq = auto.getSequence();
            sequencesToReset.add(seq);
            int expected = seq.getCurVal();
            StringBuffer value = auto.getValue();
            if (value == null) {
                throw new RuntimeException("Champ non renseigné");
            } else {
                int actual = Integer.parseInt(value.toString());
                if (expected != actual) {
                    int start = auto.getStart();
                    int end = start + auto.getLength() -1;
                    throw new InvalidNumberException("Ligne n°{}, colonnes {}-{}, - attendu : \"{}\", obtenu : \"{}\" (sequence \"{}\")",
                                    pLineNb, start, end, expected, actual, seq.getId());
                }
            }
        }
        return lineNb;
    }

    @Override
    public Iterator<AbstractRecord> iterator() {
        return new RecordsIterator(super.iterator());
    }

    public void resetSequences() {
        Iterator<AbstractRecord> iterator = iterator();
        while (iterator.hasNext()) {
            AbstractRecord record = iterator.next();
            if (record instanceof LeafRecord) {
        	List<AutonumberField> fieldsToIncrement;
                fieldsToIncrement = (List<AutonumberField> )((LeafRecord) record).getFieldsToIncrement();
                for (AutonumberField auto : fieldsToIncrement) {
                    auto.getSequence().reset();
                }
            }
        }           
    }
}
