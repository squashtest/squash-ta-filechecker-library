/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.IValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractField;

/**
 * Parent class of all the fields of Fixed Field File (FFF). Fields are components of a record  
 * 
 * @author amd
 */
public abstract class AbstractFixedField extends AbstractField {
    
    /** First character position of the field in the record */
    protected int start;

    /** Field size (number of characters) */
    protected int length;
    
    protected IValidator validator;

    /** @return The first character position of the field in the record */
    public int getStart() {
        return start;
    }

    /** @return The size of the field */
    public int getLength() {
        return length;
    }
    
    /** The type of field */
    public abstract FixedFieldType getFieldType();
    
    
    /**
     * Extract the field value from the <code>String</code> corresponding to the records.
     * 
     * @param pLine
     */
    public void extractValue(String pLine) {
        String fieldValue = pLine.substring(start - 1, start + length - 1);
        value = new StringBuffer(fieldValue);
    }
    
    @Override
    public void validate() throws InvalidSyntaxException {
        try {
            validator.validate(value);
        } catch (InvalidSyntaxException e) {
            throw new InvalidSyntaxException(e,"colonnes {}-{} : {}", start, start + length, e.getMessage());
        }
    }

}
