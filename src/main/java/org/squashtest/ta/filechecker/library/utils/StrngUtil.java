/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.utils;

public class StrngUtil {

    public static String removeSeparators(String pString){
        String lString = remove(pString, "\r");
        return remove(lString, "\n");
    }
    
    private static String remove(String pString, String pSep){
        StringBuilder ret = new StringBuilder();
        String[] bits = pString.split(pSep);
        int nb = bits.length -1;
        for (int index=0; index<nb; index++) {
            ret.append(bits[index].trim());
            ret.append(' ');
        }
        ret.append(bits[nb].trim());
        return ret.toString();
    } 
    
}
