/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.digester.Digester;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

/**
 * @author amd
 *
 */
public class SchemaUtils {
	
	/**
	 * Private constructor
	 */
	private SchemaUtils(){
	}
    
    /**
     * Retrieve the schema name referenced by the xml file given in argument
     * 
     * @param pXmlFileUrl URL of the xml file
     * @return The schema name
     * @throws IOException Exception launched if a problem occurs while trying to access the xml file  
     * @throws SAXException Exception launched if a problem occurs during the xml file reading
     */
    public static String getSchemaName(URL pXmlFileUrl) throws IOException, SAXException {

        StringBuilder schemaLocation = new StringBuilder();
        String schemaName=null;

        // Parse of the xml file given in argument in order to retrieve xsi:schemaLocation value
        final Digester digester = new Digester();
        digester.setValidating(false);
        digester.setNamespaceAware(true);
        digester.push(schemaLocation);
        Class< ? >[] classe = {String.class };
        digester.addCallMethod("*/root", "append", 1, classe);
        digester.addCallParam("*/root", 0, "xsi:schemaLocation");
        digester.parse(pXmlFileUrl);

        // Extraction of the schema name from the xsi:schemaLocation
        // xsi:schemaLocation is composed of two parts separates by a space. The schema name is inside the second part. 
        if (StringUtils.isNotBlank(schemaLocation.toString())) {
        	String[] array = schemaLocation.toString().split(" ");
        	if(array.length > 1){
        		String path = array[1].trim();
        		File schemaUrl = new File(path);
        		schemaName = schemaUrl.getName();
        	}
        } 
        return schemaName;
    }

    
}
