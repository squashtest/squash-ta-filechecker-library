/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.dao;

import java.net.URI;

/**
 * @author amd
 */
public class DatasourceFactory {

    public enum Scheme {
        file;

        public static Scheme getScheme(URI pFileURI) {

            Scheme scheme = null;
            
            String sScheme = pFileURI.getScheme();
            
            
            boolean notFound = true;
            Scheme[] schemes = values();
            int size = schemes.length;
            int index = 0;
            
            while (notFound && index < size) {
                Scheme sch = schemes[index++];
                if (sch.toString().equals(sScheme)) {
                    notFound = false;
                    scheme = sch;
                }
            }
            
            if (notFound) {
                StringBuilder buf = new StringBuilder();

                buf.append("Le protocole \"");
                buf.append(sScheme);
                buf.append("\" n'est pas géré par ce framework. Les protocoles autorisés sont : \"");
                buf.append(schemes[0].toString());
                for (int ind = 1; ind < size; ind++) {
                    buf.append("\", \"");
                    buf.append(schemes[ind].toString());
                }
                buf.append("\".");
                throw new IllegalArgumentException(buf.toString());
            }
            
            return scheme;
            
        }
    }

    public static IDatasource createDatasource(URI pFileUri, boolean pIsBinary, int pBytesPerRecord, StringBuffer pEncoding) {

        IDatasource datasource = null;
        Scheme scheme = Scheme.getScheme(pFileUri);

        switch (scheme) {
            case file:
                datasource = new LocalDatasource(pFileUri, pIsBinary, pBytesPerRecord, pEncoding);
                break;

            default:
                break;
        }
        return datasource;
    }
    
    public static IDatasource createDatasource(URI pFileUri, boolean pIsBinary, int pBytesPerRecord, StringBuffer pEncoding, int pMaxLineToRead) {

        IDatasource datasource = null;
        Scheme scheme = Scheme.getScheme(pFileUri);

        switch (scheme) {
            case file:
                datasource = new LocalDatasource(pFileUri, pIsBinary, pBytesPerRecord, pEncoding, pMaxLineToRead);
                break;

            default:
                break;
        }
        return datasource;
    }

}
