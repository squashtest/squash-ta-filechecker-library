/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import org.squashtest.ta.filechecker.library.bo.fff.formatting.IFormatter;
import org.squashtest.ta.filechecker.library.bo.fff.formatting.Padder;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.sequence.Sequence;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.IValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.RegexpValidator;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.ValidatorInitializationException;

public final class AutonumberField extends AbstractVariableFixedField<String> {

    private Sequence sequence;
    
    //Pour création avec parser
    public AutonumberField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, Sequence pSequence) throws ValidatorInitializationException {
    	label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        sequence = pSequence;
    	formatter = new Padder('0', true, pLength, false);
    	validator = new RegexpValidator("\\d{" + pLength + "}", false);
    }
    
    //Pour clonage
    /**
     * 
     */
    private AutonumberField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, Sequence pSequence, IFormatter<String> pFormatter, IValidator pValidator) {
        label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        sequence = pSequence;
        formatter = pFormatter;
        validator = pValidator;
    }
    
    
    // pour validation autonumbers
    public Sequence getSequence() {
        return sequence;
    }

    // pour être valable, ne doit être exécutée que lors du 
    // parcourt de l'arbre
    public void increment() throws InvalidSyntaxException{
        int value = sequence.getCurVal();
        setValue("" + value);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() {
        return new AutonumberField(label, description, start, length, sequence, formatter, validator);
    }

	@Override
	public FixedFieldType getFieldType() {
		return FixedFieldType.autonumber;
	}

}
