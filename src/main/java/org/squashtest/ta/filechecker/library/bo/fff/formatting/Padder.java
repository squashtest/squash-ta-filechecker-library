/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import org.apache.commons.lang.StringUtils;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

public class Padder implements IFormatter<String> {
    private int length;

    private char ch;

    private boolean nullableField;
    
    private boolean leftPadding;

    public Padder(char pChar, boolean pLeftPadding, int pLength, boolean pNullableField) {
        super();
        ch = pChar;
        leftPadding = pLeftPadding;
        nullableField = pNullableField;
        length = pLength;
    }

    public StringBuffer format(String pValue) throws InvalidSyntaxException {
        StringBuffer newValue = new StringBuffer(length);
        if (StringUtils.isBlank(pValue)) {
            if (nullableField) {
                for (int i = 0; i < length; i++) {
                    newValue.append(' ');
                }
            } else {
                throw new InvalidSyntaxException("Champ obligatoire.");
            }
        } else if (pValue.length() > length) {
            throw new InvalidSyntaxException("Le champ ne doit pas dépasser {} caractères.", length);
        } else if (leftPadding) { //left padding
            for (int i = 0; i < length - pValue.length(); i++) {
                newValue.append(ch);
            }
            newValue.append(pValue);
        } else { // right padding
            newValue.append(pValue);
            for (int i = 0; i < length - pValue.length(); i++) {
                newValue.append(ch);
            }
        }
        return newValue;
    }

}
