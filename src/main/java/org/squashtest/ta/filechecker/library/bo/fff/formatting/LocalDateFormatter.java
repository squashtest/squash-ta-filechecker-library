/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.fff.formatting;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;

/**
 * @author amd
 *
 */
public class LocalDateFormatter implements IFormatter<Date> {

    private int length;
    private boolean nullable;
    private SimpleDateFormat dateFormat;
    
    public LocalDateFormatter(String pFormat, int pLength, boolean pNullable) throws FormatterInitializationException{
        try{
            dateFormat = new SimpleDateFormat(pFormat);
        }catch(IllegalArgumentException e){
            throw new FormatterInitializationException(e, "\"{}\" n'est pas un format de date valide.", pFormat);
        }
        length = pLength;
        nullable = pNullable;
    }
    
    /* (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.bo.fff.formatting.IFormatter#format(java.lang.String)
     */
    public StringBuffer format(Date pValue) throws InvalidSyntaxException {
        StringBuffer newValue = new StringBuffer(length);
        if (pValue==null){
            if (nullable){
                for (int i = 0; i<length; i++){
                    newValue.append(' ');
                }
            }else{
                throw new InvalidSyntaxException("Champ obligatoire.");
            }
        }else{
            String date = dateFormat.format(pValue);
            if (date.length()!=length){
                throw new InvalidSyntaxException("Le format de date doit correspondre à {} caractères.", length);
            }
            newValue = new StringBuffer(date);
        }
        return newValue;
    }

}
