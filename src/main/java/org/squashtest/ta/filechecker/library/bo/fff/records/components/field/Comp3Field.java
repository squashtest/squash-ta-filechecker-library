/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.UserInput;
import org.squashtest.ta.filechecker.library.utils.mvs.MVSConversion;
import org.squashtest.ta.filechecker.library.utils.mvs.MVSDatatypeException;

public final class Comp3Field extends AbstractVariableFixedField<Double> implements UserInput {

    private int decNb;

    private StringBuffer encoding;

    private boolean nullable;

    // création initiale
    public Comp3Field(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, int pDecNb, boolean pNullable,
                    StringBuffer pEncoding) {
        label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        decNb = pDecNb;
        nullable = pNullable;
        if(pEncoding != null){
        	encoding = pEncoding;
        }
        else {
        	throw new IllegalArgumentException("Encoding can't be null for a field of type : "+getFieldType());
        }
    }

    @Override
    public Object clone() {
        return new Comp3Field(label, description, start, length, decNb, nullable, encoding);
    }

    @Override
    // pour construction de l'arbre à partir lecture fichier
    public void extractValue(String pLine) {
        // 1. Récupération du champ
        String text = pLine.substring(start - 1, start + length - 1);
        if (StringUtils.isBlank(text)) {
            if (!nullable){
                throw new IllegalArgumentException("Col. " + start + "/" + length + " : champ null non autorisé");
            }
        } else {
            try {
                // 2. Reconversion en binaire
                byte[] bytes = text.getBytes(encoding.toString());
                // 3. Décompaction comp-3 --> string;
                String unpacked = MVSConversion.comp3ToString(bytes);
                value = new StringBuffer(unpacked);
            } catch (UnsupportedEncodingException e) {
                throw new IllegalArgumentException("L'encodage \"" + encoding + "\" n'est pas pris en charge.",e);
            } catch (MVSDatatypeException e) {
                throw new IllegalArgumentException("Col. " + start + "/" + length + " : champ compacté invalide", e);
            }
        }
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :
     * <ul>
     *      <li>les décimales sont explicites</li>
     *      <li>les zéros non significatifs n'apparaissent pas</li>
     *      <li>le séparateur, lorsqu'il est présent, est un point</li>
     * </ul>
     */
    @Override
    public String toString() {
        return getFunctionalValue().toString();
    }

    /*@Override
    public String toFileFormat() {
        throw new UnsupportedOperationException();
    }*/

    /**
     * @return La valeur du champ, formattée de la manière suivante :
     * <ul>
     *      <li>les décimales sont explicites</li>
     *      <li>les zéros non significatifs n'apparaissent pas</li>
     *      <li>le séparateur, lorsqu'il est présent, est un point</li>
     * </ul>
     */
    @Override
    public StringBuffer getValue() {
        return getFunctionalValue();
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :
     * <ul>
     *      <li>les décimales sont explicites</li>
     *      <li>les zéros non significatifs n'apparaissent pas</li>
     *      <li>le séparateur, lorsqu'il est présent, est un point</li>
     * </ul>
     */
    private StringBuffer getFunctionalValue() {
        StringBuffer formattedValue = null;
        if (null != value) {
            formattedValue = new StringBuffer();
            Double numValue = Double.parseDouble(value.toString());
            Double remainder = numValue % Math.pow(10, decNb);
            numValue /= Math.pow(10, decNb);
            if (remainder.equals(0d)) {
                formattedValue.append(numValue.longValue());
            } else {
                formattedValue.append(numValue);
            }
        }
        return formattedValue;
    }

    @Override
    public void setValue(Double pValue) throws InvalidSyntaxException {
        String longValue = "" + (long ) Math.floor(pValue.doubleValue() * Math.pow(10, decNb));
        value = new StringBuffer(longValue);
    }

    @Override
    public void validate() throws InvalidSyntaxException {
    }

	@Override
	public FixedFieldType getFieldType() {
		return FixedFieldType.comp3;
	}

}
