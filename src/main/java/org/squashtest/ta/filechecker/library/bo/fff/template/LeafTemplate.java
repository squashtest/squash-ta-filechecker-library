/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.template;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.field.IdField;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;

public class LeafTemplate {

    public static final Logger LOGGER = LoggerFactory.getLogger(LeafTemplate.class);

    private LeafRecord prototype;

    /** Liste des champs qui font figure d'identifiants pour l'enregistrement. */
    private List<IdField> ids;

    /**
     * Nombre de caractères de l'enregistrement feuille
     */
    private int length;

    /**
     * 
     * @param pPrototype
     * @param pIds
     * @param pLength
     */
    public LeafTemplate(LeafRecord pPrototype, List<IdField> pIds, int pLength) {
        prototype = pPrototype;
        ids = pIds;
        length = pLength;
    }

    // pour treebuilder
    public int getLength() {
        return length;
    }

    /**
     * @param pLine
     * @return
     */
    public boolean matches(String pLine) {
        // NB : la validation schematron garantit qu'il y a au moins un
        // identifiant par enregistrement feuille
        boolean match = true;
        IdField id;
        if (pLine.length() >= length) {
            Iterator<IdField> idIterator = ids.iterator();
            while (match && idIterator.hasNext()) {
                id = idIterator.next();
                String idValue = id.getValue().toString();
                int idStart = id.getStart();
                if (!pLine.substring(idStart - 1, idStart + id.getLength() - 1).equals(idValue)) {
                    match = false;
                };
            }
        } else {
            match = false;
        }
        return match;
    }

    /**
     * @return
     */
    public LeafRecord clonePrototype() {
        return (LeafRecord ) prototype.clone();
    }

}
