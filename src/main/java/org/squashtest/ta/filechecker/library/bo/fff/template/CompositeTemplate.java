/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.template;

import java.util.List;

import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.CompositeRecord;
import org.squashtest.ta.filechecker.library.bo.fff.records.components.record.LeafRecord;

public class CompositeTemplate {
    
    private String openingName;
    
    private String closingName;
    
    private List<String> descendants;
    
    private CompositeRecord prototype;
    
    /**
     * @param pOpeningName
     * @param pClosingName
     * @param pDescendants
     * @param pPrototype
     */
    public CompositeTemplate(String pOpeningName, String pClosingName, List<String> pDescendants,
                    CompositeRecord pPrototype) {
        openingName = pOpeningName;
        closingName = pClosingName;
        descendants = pDescendants;
        prototype = pPrototype;
    }
    
    public String getOpeningName() {
        return openingName;
    }

    public String getClosingName() {
        return closingName;
    }

    public List<String> getDescendants() {
        return descendants;
    }
    
    public CompositeRecord getCompositeWithOpening (LeafRecord pOpeningRecord){
        CompositeRecord composite = (CompositeRecord ) prototype.clone();
        composite.setOpeningRecord(pOpeningRecord);
        return composite;
    }
    
}
