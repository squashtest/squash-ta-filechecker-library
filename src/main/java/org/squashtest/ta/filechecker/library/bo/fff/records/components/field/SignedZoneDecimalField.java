/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.components.field;

import java.io.UnsupportedEncodingException;

import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.UserInput;
import org.squashtest.ta.filechecker.library.utils.mvs.MVSDatatypeException;

public final class SignedZoneDecimalField extends AbstractVariableFixedField<Double> implements UserInput {

    private int decNb;

    private StringBuffer encoding;

    // création initiale + clonage
    public SignedZoneDecimalField(StringBuffer pLabel, StringBuffer pDescription, int pStart, int pLength, int pDecNb,
                    StringBuffer pEncoding) {
        label = pLabel;
        description = pDescription;
        start = pStart;
        length = pLength;
        decNb = pDecNb;
        if(pEncoding != null){
        	encoding = pEncoding;
        }
        else {
        	throw new IllegalArgumentException("Encoding can't be null for a field of type : "+getFieldType());
        }
    }

    @Override
    public Object clone() {
        return new SignedZoneDecimalField(label, description, start, length, decNb, encoding);
    }

    @Override
    // pour construction de l'arbre à partir lecture fichier
    public void extractValue(String pLine) {
        // 1. Récupération du champ
        String text = pLine.substring(start - 1, start + length - 1);
        try {
            // 3. Conversion/decompaction
            String converted = conversion(text);
            value = new StringBuffer(converted);
        } catch (UnsupportedEncodingException | MVSDatatypeException e) {
            throw new IllegalArgumentException(e.getMessage(),e);
        }
    }

    private String conversion(String pText) throws UnsupportedEncodingException {
        int len = pText.length();
        int nibble;
        StringBuilder buf = new StringBuilder(len + 1);
        // Récupérons le dernier caractère = octet qui contient le signe et le
        // dernier chiffre
        String last = pText.substring(len - 1);
        byte lastByte = last.getBytes(encoding.toString())[0];
        // a. Demi-octet gauche = signe
        // On veut la valeur du premier demi-octet : on décale les bits
        // quatre fois vers la droite, en remplaçant les bits manquant à
        // gauche par des zéros.
        nibble = (lastByte & 0xf0) >>> 4;
        switch (nibble) {
            case 13:
            case 10:
                buf.append("-");
                break;
            default:
                break;
        }
        buf.append(pText.substring(0, len - 1));
        // Demi-octet droit = dernier chiffre
        nibble = (lastByte & 0x0f);
        buf.append(nibble);
        return buf.toString();
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :
     * <ul>
     *   <li>les décimales sont explicites</li>
     *   <li>les zéros non significatifs n'apparaissent pas</li>
     *   <li>le séparateur, lorsqu'il est présent, est un point</li>
     * </ul>
     */
    @Override
    public String toString() {
        return getFunctionalValue().toString();
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :
     * <ul>
     *   <li>les décimales sont explicites</li>
     *   <li>les zéros non significatifs n'apparaissent pas</li>
     *   <li>le séparateur, lorsqu'il est présent, est un point</li>
     * </ul>
     */
    @Override
    public StringBuffer getValue() {
        return getFunctionalValue();
    }

    /**
     * @return La valeur du champ, formattée de la manière suivante :
     * <ul>
     *   <li>les décimales sont explicites</li>
     *   <li>les zéros non significatifs n'apparaissent pas</li>
     *   <li>le séparateur, lorsqu'il est présent, est un point</li>
     * </ul>
     */
    private StringBuffer getFunctionalValue() {
        StringBuffer formattedValue = null;
        if (null != value) {
            formattedValue = new StringBuffer();
            Double numValue = Double.parseDouble(value.toString());
            Double remainder = numValue % Math.pow(10, decNb);
            numValue /= Math.pow(10, decNb);
            if (remainder.equals(0d)) {
                formattedValue.append(numValue.longValue());
            } else {
                formattedValue.append(numValue);
            }
        }
        return formattedValue;
    }

    @Override
    public void setValue(Double pValue) throws InvalidSyntaxException {
        // Double --> nombre entier sous forme de chaîne de caractères
        String longValue = "" + (long ) Math.floor(pValue.doubleValue() * Math.pow(10, decNb));
        value = new StringBuffer(longValue);
    }
    
    @Override
    public void validate() throws InvalidSyntaxException {
    }

	@Override
	public FixedFieldType getFieldType() {
		return FixedFieldType.signedZoneDecimal;
	}
    
}
