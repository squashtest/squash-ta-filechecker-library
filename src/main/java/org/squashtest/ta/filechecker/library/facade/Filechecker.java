/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.facade;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.records.validation.syntax.InvalidSyntaxException;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsBuilder;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.iface.Records;
import org.squashtest.ta.filechecker.library.dao.DatasourceFactory;
import org.squashtest.ta.filechecker.library.dao.IDatasource;
import org.squashtest.ta.filechecker.library.utils.ConfigurationException;
import org.squashtest.ta.filechecker.library.utils.xpath.InvalidContentException;
import org.squashtest.ta.filechecker.library.utils.xpath.InvalidXpathQueriesException;
import org.squashtest.ta.filechecker.library.utils.xpath.XpathAssertions;
import org.xml.sax.SAXException;

/**
 * @author amd
 */
public class Filechecker {
    private static final Logger LOGGER = LoggerFactory.getLogger(Filechecker.class);
    private static final String NO_RECORD_ERR = "Pas d'enregistrements en mémoire.";

    private IDatasource datasource;

    private AbstractRecordsTemplate template;

    private Records records;
    
    public void loadFile(URI pFileUri, URI pDescriptorFile) throws FileNotFoundException, IOException,
                    SAXException, InvalidDescriptorException {
        template = TemplateFactory.createTemplate(pDescriptorFile);
        datasource = DatasourceFactory.createDatasource(pFileUri, template.isBinary(), template.getBytesPerRecord(),
                                        template.getEncoding());
        AbstractRecordsBuilder builder = RecordsBuilderFactory.createBuilder(datasource, template);
        records = builder.buildRecords();
    }

    public void loadFile(URI pFFFUri, URI pDescriptorFile, int pMaxRecordNb) throws FileNotFoundException, IOException,
                    SAXException, InvalidDescriptorException {
        template = TemplateFactory.createTemplate(pDescriptorFile);
        datasource = DatasourceFactory.createDatasource(pFFFUri, template.isBinary(), template.getBytesPerRecord(),
                                        template.getEncoding(), pMaxRecordNb);
        AbstractRecordsBuilder builder = RecordsBuilderFactory.createBuilder(datasource, template);
        records = builder.buildRecords();
    }

    public void validateSequences() throws InvalidSyntaxException {
        if (records != null) {
            records.validateSequences();
        }
    }

    public void validateStructureAndSyntax() throws InvalidSyntaxException {
        if (records != null) {
            records.validate();
        }
    }

    public void validateSemantics(URI pXpathQueriesFileURI) throws InvalidContentException, IOException {
        if(records!=null)
        {
    	try {
	            XpathAssertions assertions = new XpathAssertions(pXpathQueriesFileURI.toURL());
	            records.validateSemantics(assertions.iterator());
	        } catch (InvalidXpathQueriesException | SAXException e) {
	        	throw new ConfigurationException(e,"Le fichier {} est incorrect.", pXpathQueriesFileURI);
                }
        }else{
        	throw new UnsupportedOperationException(NO_RECORD_ERR);
        }

    }

    public Records getRecords() {
        return records;
    }

}
