/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.facade;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.xml.sax.SAXException;

import org.squashtest.ta.filechecker.library.bo.fff.records.builder.FFFrecordsBuilder;
import org.squashtest.ta.filechecker.library.bo.fff.template.FFFrecordsTemplate;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsBuilder;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsTemplate;
import org.squashtest.ta.filechecker.library.dao.IDatasource;

/**
 * @author amd
 */
public class RecordsBuilderFactory {

    public static AbstractRecordsBuilder createBuilder(IDatasource datasource, AbstractRecordsTemplate pRecordsTemplate)
                    throws FileNotFoundException, IOException {
       
        AbstractRecordsBuilder builder = null;
        FileType fileType = pRecordsTemplate.getType();

        switch (fileType) {
            
            case FixedFieldFile:
                FFFrecordsTemplate template = (FFFrecordsTemplate ) pRecordsTemplate;
                builder = new FFFrecordsBuilder(datasource, template);
                break;

            default:
                throw new UnsupportedOperationException("Le type de fichier " + fileType + " n'est pas pris en charge.");
            
        }
        
        return builder;
        
    }

}
