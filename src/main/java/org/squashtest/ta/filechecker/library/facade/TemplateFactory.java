/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.facade;

import java.io.IOException;
import java.net.URI;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.DescriptorChecker;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.DescriptorsSchemas;
import org.squashtest.ta.filechecker.library.bo.descriptor.checker.InvalidDescriptorException;
import org.squashtest.ta.filechecker.library.bo.fff.descriptor.parser.FFFdescriptorParser;
import org.squashtest.ta.filechecker.library.bo.iface.AbstractRecordsTemplate;
import org.squashtest.ta.filechecker.library.utils.SchemaUtils;
import org.xml.sax.SAXException;

/**
 * @author amd
 */
public class TemplateFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(TemplateFactory.class);
    
    private static DescriptorsSchemas descriptorSchemas;

    /**
     * @param pDescriptorFileURI URI of the descriptor file.
     * @return
     * @throws IOException si une erreur survient :
     * <ul>
     *   <li>lors de l'accès au fichier de description</li>
     *   <li>lors de l'accès au schéma de validation du fichier de
     *             description</li>
     *   <li>lors de l'accès au fichier de mapping schéma de
     *             descripteur / type de fichier décrit par le descripteur</li>
     * </ul>
     * @throws SAXException si une erreur survient lors de la validation/du parsing du descripteur
     * @throws InvalidDescriptorException 
     */
    public static AbstractRecordsTemplate createTemplate(URI pDescriptorFileURI) throws IOException, SAXException, InvalidDescriptorException {

        descriptorSchemas = DescriptorsSchemas.getInstance();
        AbstractRecordsTemplate template = null;
        
        URL descriptorUrl = pDescriptorFileURI.toURL();
        
        // Retrieving the schema name 
        String schemaName;
        try {
            schemaName = SchemaUtils.getSchemaName(descriptorUrl);
        } catch (SAXException e) {
            throw new InvalidDescriptorException(e, "Unable to parse the file \"" + descriptorUrl + "\".");
        }
        if (StringUtils.isBlank(schemaName)) {
            throw new InvalidDescriptorException("The file \" {} \" should reference a schema.", descriptorUrl);
        }

        // Descriptor file schema URL
        URL schemaUrl = descriptorSchemas.getSchemaUrl(schemaName);
        if(schemaUrl==null)
        {
        	throw new InvalidDescriptorException("XSD schema {} is not recognized. Please check that this schema exists and is supported by filechecker", schemaName);
        }
        
        // Descriptor file xsd and schematron validation
        DescriptorChecker descriptorChecker = new DescriptorChecker(descriptorUrl, schemaUrl);
        descriptorChecker.validate();

        // Template creation according to the file type
        FileType fileType = descriptorSchemas.getFileType(schemaName);
        switch (fileType) {
            case FixedFieldFile:
                template = FFFdescriptorParser.getRecordsTemplate(descriptorUrl);
                break;
            default:
                throw new UnsupportedOperationException("The files of type " + fileType + " are not supported.");
        }

        return template;
    }

}
