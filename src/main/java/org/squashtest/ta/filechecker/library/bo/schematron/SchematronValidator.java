/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/**
 * 
 */
package org.squashtest.ta.filechecker.library.bo.schematron;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.digester.Digester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import org.squashtest.ta.filechecker.library.utils.Constants;

/**
 * @author amd
 */
public class SchematronValidator {

    private static SchematronValidator uniqueInstance;

    private URL XSD2SchtrnUrl;

    private URL svrlUrl;

    private TransformerFactory transformerFactory;

    private SchematronValidator() {
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
        transformerFactory = TransformerFactory.newInstance();
        XSD2SchtrnUrl = Thread.currentThread().getContextClassLoader().getResource(Constants.XSD_2_SCHMTRN_STYLESHEET);
        svrlUrl = Thread.currentThread().getContextClassLoader().getResource(Constants.SVRL_STYLESHEET);
    }

    public static SchematronValidator getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new SchematronValidator();
        }
        return uniqueInstance;
    }
    
    public List<String> validate(URL pXml2Validate, URL pValidationSchema, boolean pXsdEmbedded) throws SAXException, IOException {
        StreamSource styleSheet = getValidationStyleSheet(pValidationSchema, pXsdEmbedded);
        StreamSource input = new StreamSource(pXml2Validate.toString());
        String result = transform(input, styleSheet);
        return analyse(result);
    }
    
    //XXX pour validation fonctionnelle des champs avec schematron
    public List<String> validate(StreamSource pXml2Validate, URL pValidationStyleSheet) throws IOException, SAXException {
        StreamSource styleSheet = new StreamSource(pValidationStyleSheet.toString());
        StreamSource input = pXml2Validate;
        String result = transform(input, styleSheet);
        return analyse(result);
    }

    /**
     * @param pXml2Validate
     * @return
     */
    private StreamSource getValidationStyleSheet(URL pValidationSchema, boolean pXsdEmbedded) {

        StreamSource input;
        StreamSource transformationStyleSheet;
        StreamSource validationStyleSheet;
        String result;

        // 1. Obtention du sch�ma sch�matron
        if (pXsdEmbedded){
            //Extraction du sch�ma Schematron encapsul� dans le sch�ma XSD
            input = new StreamSource(pValidationSchema.toString());
            transformationStyleSheet = new StreamSource(XSD2SchtrnUrl.toString());
            result = transform(input, transformationStyleSheet);
            input = new StreamSource(new StringReader(result));
        }else{
            input = new StreamSource(pValidationSchema.toString());
        }

        // 2. Transformation du schéma Schematron en feuille de style XSLT
        // de validation
        transformationStyleSheet = new StreamSource(svrlUrl.toString());
        result = transform(input, transformationStyleSheet);
        validationStyleSheet = new StreamSource(new StringReader(result));

        return validationStyleSheet;
    }



    private String transform(StreamSource pStrmInput, StreamSource pStyleSheetSrc) {
        StringWriter resultWriter = new StringWriter();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer(pStyleSheetSrc);
            // pour que les chemins des éléments non valides soient écrits de
            // manière intelligible pour un humain.
            transformer.setParameter("full-path-notation", "2");
            transformer.transform(pStrmInput, new StreamResult(resultWriter));
        } catch (TransformerConfigurationException e1) {
            LOGGER.error("Wrong transformer configuration",e1);
        } catch (TransformerException e) {
            LOGGER.error("Transform failure",e);
        }
        return resultWriter.toString();
    }
    public static final Logger LOGGER = LoggerFactory.getLogger(SchematronValidator.class);

    /**
     * @param pResult
     * @return Une <code>List</code> de messages d'erreurs
     */
    private List<String> analyse(String pSchematronResult) throws SAXException, IOException {
        Digester d = new Digester();
        d.setValidating(false);
        d.setNamespaceAware(true);
        List<SchmtrnFailure> failureList = new ArrayList<SchmtrnFailure>();
        d.push(failureList);
        d.addObjectCreate("*/failed-assert", SchmtrnFailure.class);
        d.addSetNext("*/failed-assert", "add");
        d.addSetProperties("*/failed-assert");
        d.addSetNestedProperties("*/failed-assert");
        d.parse(new StringReader(pSchematronResult));
        List<String> list = new ArrayList<String>();
        for (SchmtrnFailure fail : failureList) {
            StringBuilder msg = new StringBuilder();
            msg.append(fail.getLocation());
            msg.append(" : ");
            msg.append(fail.getText());
            list.add(msg.toString());
        }
        return list;
    }

}
