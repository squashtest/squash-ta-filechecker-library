/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.filechecker.library.bo.fff.records.validation.structure;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LitteralExpression extends AbstractExpression {

    private String recordName;

    public static final Logger LOGGER = LoggerFactory.getLogger(LitteralExpression.class);

    // pour parser
    public LitteralExpression() {
        super();
    }

    // pour parser
    public void setRecordName(String pRecordName) {
        recordName = pRecordName;
    }
    
    //pour getChildren
    public String getRecordName() {
        return recordName;
    }

    /* (non-Javadoc)
     * @see org.squashtest.ta.filechecker.library.bo.sqtl.tree.interpreter.IExpression#getPattern()
     */
    @Override
    public String getPattern(String separator) {
    	StringBuilder builder = new StringBuilder();
    	builder.append(separator);
    	builder.append(recordName);
    	builder.append(separator);
        return builder.toString();
    }

    @Override
    public List<String> getOperands() {
        List<String> list = new ArrayList<String>();
        list.add(recordName);
        return list;
    }

}
